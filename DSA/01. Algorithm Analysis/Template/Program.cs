﻿using System;

namespace AlgorithmAnalysis
{
	class Program
	{
		static void Main()
		{
		}

		// Returns the product of two numbers.
		static int Product(int a, int b)            // Complexity O(n)
		{
			int sum = 0;// O(1)

			for (int i = 0; i < b; i++)// O(n)
			{
				sum += a;// O(1)
			}

			return sum;
		}

		// Returns a to the power of b			
		static int Power(int a, int b)          // Complexity O(n)
		{
			if (b < 0)// O(1)
			{
				return 0;
			}

			if (b == 0)// O(1)
			{
				return 1;
			}

			int power = a;// O(1)

			while (b > 1)// O(n)
			{
				power *= a;// O(1)
				b--;// O(1)
			}

			return power;
		}

		// Returns the remainder after dividing a by b
		static int Mod(int a, int b)            // Complexity O(1)
		{
			if (b < 0)
			{
				return -1;
			}

			int div = a / b;
			return a - div * b;
		}

		static int Sum3(int n)			// Complexity O(n^3)
		{
			int sum = 0;
			for (int a = 0; a < n; a++)
			{
				for (int b = 0; b < n; b++)
				{
					for (int c = 0; c < n; c++)
					{
						sum += (a * b * c);
					}
				}
			}
			return sum;
		}

		static int SumNM(int n, int m)          // Complexity O(n^2)
		{
			int sum = 0;
			for (int a = 0; a < n; a++)
			{
				for (int b = 0; b < m; b++)
				{
					sum += (a * b);
				}
			}
			return sum;
		}

		// Determines if a number is even.
		static bool IsEven(int number)          // Complexity O(1)
		{
			return number % 2 == 0;
		}

		// Determines if a number is odd.
		static bool IsOdd(int number)           // Complexity O(1)
		{
			bool result = number % 2 != 0;
			return result;
		}

		// Returns the first element of an array.
		static int GetFirstElement(int[] array)         // Complexity O(1)
		{
			int element = array[0];
			return element;
		}

		// Returns the last element of an array.
		static int GetLastElement(int[] array)          // Complexity O(1)
		{
			int element = array[array.Length - 1];          
			return element;
		}

		// Returns the element at the given index.
		static int GetElementByIndex(int[] array, int index)            // Complexity O(1)
		{
			int element = array[index];
			return element;
		}

		// Finds the maximum value from an unsorted array.
		static int FindMaxElement(int[] array)          // Complexity O(n)
		{
			int max = int.MinValue;

			for (int i = 0; i < array.Length; i++)
			{
				if (array[i] > max)
				{
					max = array[i];
				}
			}

			return max;
		}

		// Determines whether an element exists in an unsorted array.
		static bool Contains(int[] array, int element)          // Complexity O(n)
		{
			for (int i = 0; i < array.Length; i++)
			{
				if (array[i] == element)
				{
					return true;
				}
			}

			return false;
		}

		// Finds the index of an element in a sorted array.
		static int GetElementIndex(int[] array, int elementToFind)          // Complexity O(n^2)
		{
			// array must be sorted!
			Array.Sort(array);// O(n^2)

			// Perform binary search
			int leftIndex = 0;
			int rightIndex = array.Length - 1;

			while (leftIndex <= rightIndex)// O(n)
			{
				int middleIndex = leftIndex + (rightIndex - leftIndex) / 2;
				int middleElement = array[middleIndex];

				// Check if the element is at the middle
				if (middleElement == elementToFind)
				{
					return middleIndex;
				}

				// If the element is greater than the element in the middle, ignore the left half 
				if (elementToFind > middleElement)
				{
					leftIndex = middleIndex + 1;
				}
				// If the element is smaller than the element in the middle, ignore the right half 
				else
				{
					rightIndex = middleIndex - 1;
				}
			}

			// The element has not been found, return -1
			return -1;
		}

		// Checks if an array has duplicate values.
		static bool HasDuplicates(int[] numbers)            // Complexity O(n^2)
		{
			for (int outerIndex = 0; outerIndex < numbers.Length; outerIndex++)
			{
				for (int innerIndex = 0; innerIndex < numbers.Length; innerIndex++)
				{
					if (outerIndex == innerIndex)
					{
						continue;
					}

					if (numbers[outerIndex] == numbers[innerIndex])
					{
						return true;
					}
				}
			}

			return false;
		}

		// Returns the Fibonacci number on the n-th position using an iterative approach.
		static int GetFibonacciIterative(int n)         // Complexity O(n)
		{
			if (n < 0)
			{
				return 0;
			}

			if (n < 2)
			{
				return n;
			}

			int prev = 0;
			int result = 1;

			for (int i = 1; i < n; i++)
			{
				int temp = result;
				result += prev;
				prev = temp;
			}

			return result;
		}

		// Returns the Fibonacci number on the n-th position using a recursive approach.
		static int GetFibonacciRecursive(int n)         // Complexity O(1)
		{
			if (n <= 1)
				return 1;

			return GetFibonacciRecursive(n - 1) + GetFibonacciRecursive(n - 2);
		}
	}
}
