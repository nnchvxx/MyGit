﻿using System;
using System.Collections.Generic;

namespace DSA.Linear
{
    public class LinkedList<T>
    {
        public LinkedList(T[] array)
        {
            if (array==null)
            {
                throw new ArgumentNullException("Can't create a list from a null array.");
            }
            for (int i = array.Length-1; i >=0; i--)
            {
                this.AddFirst(array[i]);
            }
        }
        public ListNode<T> Head { get; private set; }
        public ListNode<T> AddFirst(T value)
        {
            var node = new ListNode<T>(value);
            node.Next = this.Head;
            this.Head = node;
            return node;
        }

        public T RemoveFirst()
        {
            if (this.Head==null)
            {
                throw new System.NullReferenceException("List is empty.");
            }
            T value = this.Head.Value;
            this.Head = this.Head.Next;
            return value;
        }

        public ListNode<T> AddAfter(ListNode<T> node, T value)
        {
            var newNode = new ListNode<T>(value);
            newNode.Next = node.Next;
            node.Next = newNode;
            return newNode;
        }

        public void RemoveAfter(ListNode<T> node)
        {
            if (node == null)
            {
                throw new NullReferenceException("Can't remove after a null node.");
            }
            if (node.Next!=null)
            {
                node.Next = node.Next.Next;
            }
        }

        public ListNode<T> Find(T value)
        {
            var current = this.Head;
            while (current!=null)
            {
                if (current.Value.Equals(value))
                {
                    return current;
                }
                current = current.Next;
            }
            return null;
        }
        public IEnumerator<T> GetEnumerator()
        {
            ListNode<T> current = this.Head;
            while (current != null)
            {
                yield return current.Value;
                current = current.Next;
            }
        }


    }
}
