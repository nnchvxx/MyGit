﻿using System;

namespace DSA.Linear
{
    class Program
    {
        static void Main()
        {
            var list = new LinkedList<int>(new int[] { 1,2,3,4,5,6,7,8,9,10});
            int M = 1;
            int N = 1;

            var current = list.Head;
            while (current != null)
            {
                for (int count = 1; count < M && current != null; count++)
                {
                    current = current.Next;
                }
                if (current == null)
                {
                    break;
                }
                for (int count = 1; count <= N && current.Next != null; count++)
                {
                    list.RemoveAfter(current);
                }
                current = current.Next;
            }

            foreach (int item in list)
            {
                Console.WriteLine(item);
            }


            /*foreach (int item in list)
                Console.WriteLine(item);

            Console.WriteLine("Remove after 2");

            ListNode<int> nodeToRemoveAfter = list.Find(2);
            list.RemoveAfter(nodeToRemoveAfter);

            foreach (int item in list)
                Console.WriteLine(item);*/

        }
    }
}
