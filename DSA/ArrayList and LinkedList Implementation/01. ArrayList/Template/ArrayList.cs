﻿using System.Collections.Generic;

namespace DSA.Linear
{
    public class ArrayList<T>
    {
        private T[] items;
        private int count;
        public ArrayList()
        {
            this.items = new T[4];
            this.count = 0;
        }
        public void Add(T value)
        {
            this.EnsureCapacity();
            this.items[this.count] = value;
            this.count++;
        }

        public void Insert(int index, T value)
        {
            this.ValidateIndex(index);
            this.EnsureCapacity();
            System.Array.Copy(this.items, index, this.items, index + 1, this.count - index);
            this.items[index] = value;
            this.count++;
        }

        public void RemoveAt(int index)
        {
            this.ValidateIndex(index);
            this.count--;
            System.Array.Copy(this.items, index + 1, this.items, index, this.count - index);
            this.items[this.count] = default;
        }

        public T Get(int index)
        {
            this.ValidateIndex(index);
            return this.items[index];
        }

        public void Set(int index, T value)
        {
            this.ValidateIndex(index);
            this.items[index] = value;
        }

        private void EnsureCapacity()
        {
            if (this.Count == this.Capacity)
            {
                T[] newItems = new T[this.Capacity * 2];
                System.Array.Copy(this.items, newItems, this.Count);
                this.items = newItems;
            }
        }
        private void ValidateIndex(int index)
        {
            if (index < 0 || index >= this.items.Length)
            {
                throw new System.ArgumentException($"Index {index} is out of range.");
            }

        }
        public int Count
        {
            get => this.count;
        }

        public int Capacity
        {
            get => this.items.Length;
        }
        public T this[int index]
        {
            get
            {
                return this.Get(index);
            }
            set
            {
                this.Set(index, value);
            }
        }
        public IEnumerator<T> GetEnumerator()
        {
            for (int index = 0; index < this.count; index++)
            {
                yield return this.items[index];
            }
        }



    }
}
