﻿using System;

namespace DSA.Linear
{
    class Program
    {
        static void Main()
        {
            var list = new ArrayList<int>();
            list.Add(12);
            list.Add(15);
            list.Add(17);
            list.Add(1);
            list.Add(10);
            list.Add(17);
            int max = int.MinValue;
            int maxIndex = -1;

            for (int index = 0; index < list.Count; index++)
            {
                int current = list[index];

                if (current > max)
                {
                    max = current;
                    maxIndex = index;
                }
            }

            list.RemoveAt(maxIndex);
            list.Insert(0, max);

            foreach (int element in list)
            {
                Console.Write($" {element} ");
            }


            /* Console.WriteLine("Test Count and Capacity");
             var list = new ArrayList<int>();
             Console.WriteLine($"  ArrayList created, Count: {list.Count}, Capacity: {list.Capacity}");
             list.Add(12);
             Console.WriteLine($"  Add(12), Count: {list.Count}, Capacity: {list.Capacity}");
             list.Add(15);
             Console.WriteLine($"  Add(15), Count: {list.Count}, Capacity: {list.Capacity}");
             list.Add(17);
             Console.WriteLine($"  Add(17), Count: {list.Count}, Capacity: {list.Capacity}");
             list.Add(1);
             Console.WriteLine($"  Add(1), Count: {list.Count}, Capacity: {list.Capacity}");
             list.Add(10);
             Console.WriteLine($"  Add(10), Count: {list.Count}, Capacity: {list.Capacity}");
             list.Add(17);
             Console.WriteLine($"  Add(17), Count: {list.Count}, Capacity: {list.Capacity}");

             Console.WriteLine("Indexer test");
             Console.WriteLine($"  list[0] == 12? {list[0] == 12}");
             Console.WriteLine($"  list[1] == 15? {list[1] == 15}");
             Console.WriteLine($"  list[2] == 17? {list[2] == 17}");
             Console.WriteLine($"  list[3] == 1? {list[3] == 1}");
             Console.WriteLine($"  list[4] == 10? {list[4] == 10}");
             Console.WriteLine($"  list[5] == 17? {list[5] == 17}");

             Console.WriteLine("Iteration test");
             Console.WriteLine("  Using the list in foreach loop");
             foreach (var element in list)
                 Console.WriteLine($"    Element: {element}");

             Console.WriteLine("  Using the list in FOR loop");
             for (int index = 0; index < list.Count; index++)
                 Console.WriteLine($"    Index: {index}, Element: {list[index]}");
 */
        }
    }
}