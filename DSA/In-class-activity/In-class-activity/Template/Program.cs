﻿using System;
using System.Linq;
using System.Text;

namespace HtmlWriter
{
    class Program
    {
        static void Main()
        {
            var rootNode = GetRootNode();
            int indent = 0;

            WriteHtml(rootNode, indent);
        }

        static void WriteHtml(HtmlNode rootNode, int indent)
        {
            // base case

            if (rootNode.ChildNodes.Count == 0)
            {
                // print on the same line
                Console.Write(new string(' ', indent));
                Console.Write(OpenTag(rootNode));
                Console.WriteLine(CloseTag(rootNode));

                // call recurse
            }
            else
            {
                // indent + 4
                // open tag
                // foreach ->
                Console.Write(new string(' ', indent));
                Console.WriteLine(OpenTag(rootNode));

                foreach (var node in rootNode.ChildNodes)
                {
                    WriteHtml(node, indent + 4);
                    
                }
                //close tag
                Console.Write(new string (' ', indent));
                Console.WriteLine(CloseTag(rootNode));

            }
        }

        private static HtmlNode GetRootNode()
        {
            HtmlNode root = new HtmlNode("html");

            HtmlNode head = new HtmlNode("head");
            root.ChildNodes.Add(head);

            HtmlNode title = new HtmlNode("title");
            head.ChildNodes.Add(title);

            HtmlNode body = new HtmlNode("body");
            root.ChildNodes.Add(body);
            body.Attributes.Add(new HtmlAttribute("class", "container"));

            HtmlNode div1 = new HtmlNode("div");
            body.ChildNodes.Add(div1);
            div1.Attributes.Add(new HtmlAttribute("class", "navbar-container"));
            div1.Attributes.Add(new HtmlAttribute("id", "navbar"));

            HtmlNode div2 = new HtmlNode("div");
            body.ChildNodes.Add(div2);
            div2.Attributes.Add(new HtmlAttribute("class", "main-container"));
            div2.Attributes.Add(new HtmlAttribute("id", "main-container"));

            return root;
        }

        private static string OpenTag(HtmlNode node)
        {
            StringBuilder sb = new StringBuilder($"<{node.Name}");


            if (node.Attributes.Count != 0)
            {
                foreach (var attribute in node.Attributes)
                {
                    sb.Append($" {attribute.Name}=\"{attribute.Value}\"");
                }
            }

            return sb.Append(">").ToString();
        }
        private static string CloseTag(HtmlNode node)
        {
            return $"</{node.Name}>";
        }
    }
}
