﻿using System;
using System.Linq;

namespace SolveExpression
{
    class Program
    {
        static void Main(string[] args)
        {
            string expression = Console.ReadLine();

            Console.WriteLine(Solve(expression));
        }

        static decimal Solve(string expression)
        {
            var leftNumber = string.Concat(expression.TakeWhile(c => c != '+' && c != '-' && c != '*' && c != '/'));

            char operation = '\u0000';

            if (expression.Length != leftNumber.Length)
            {
                operation = expression[leftNumber.Length];
            }

            if (!expression.Contains(')'))
            {
                if (!expression.Contains('+') && !expression.Contains('-') && !expression.Contains('*') && !expression.Contains('/'))
                {
                    return decimal.Parse(expression);
                }

                if (operation == '+')
                {
                    return decimal.Parse(leftNumber) + decimal.Parse(expression.Substring(leftNumber.Length + 1));
                }
                if (operation == '-')
                {
                    return decimal.Parse(leftNumber) - decimal.Parse(expression.Substring(leftNumber.Length + 1));
                }
                if (operation == '*')
                {
                    return decimal.Parse(leftNumber) * decimal.Parse(expression.Substring(leftNumber.Length + 1));
                }
                if (operation == '/')
                {
                    return decimal.Parse(leftNumber) / decimal.Parse(expression.Substring(leftNumber.Length + 1));
                }
            }

            if (operation == '+')
            {
                return decimal.Parse(leftNumber) + Solve(expression.Substring(leftNumber.Length + 2, expression.Length - leftNumber.Length - 3));
            }
            else if (operation == '-')
            {
                return decimal.Parse(leftNumber) - Solve(expression.Substring(leftNumber.Length + 2, expression.Length - leftNumber.Length - 3));
            }
            else if (operation == '*')
            {
                return decimal.Parse(leftNumber) * Solve(expression.Substring(leftNumber.Length + 2, expression.Length - leftNumber.Length - 3));
            }
            else 
            {
                return decimal.Parse(leftNumber) / Solve(expression.Substring(leftNumber.Length + 2, expression.Length - leftNumber.Length - 3));
            }
        }
    }
}
