<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

# BoardR - Task Organizing System
_Part 6_

### 1. Description

**BoardR** is a task-management system which will evolve in the next several weeks. During the course of the project, we will follow the best practices of `Object-Oriented Programming` and `Design`. 

### 2. Project information
- Framework: **.NET Core 3.0**
- Language: **C# 8**

### 3. Goals
Your task is to provide an alternative implementation for the `ILogger` interface which logs data on a text file on your computer. After that we will implement an additional helper class which handles the instantiation of all logger implementations.
You will achieve this by applying **Inversion of Control** through the **Dependency Inversion** principle.

> **Notes:** In our last activity we were passing an `ILogger` to `LogHistory()`. By doing this we've actually achieved **Dependency Inversion** as the `Board` itself is not responsible for the instantiation of a logger. Instead an abstraction is passed as a parameter and all the `Board` class requires is for this abstraction to have a `Log()` method, which is guaranteed as it is included in the `ILogger` signature and as we know already, interfaces are contracts and implementations must be present. This all alows us to write decoupled code as the `Board` class **does not depend on an exact implementation**, instead **it depends on an abstraction**. Passing a dependency through a method is called **Method Injection** - one of the ways to achieve **Dependency Inversion**.

```cs
public static class Board
{
      // any class implementing ILogger can be passed as a parameter due to the abstraction provided by the interface
        public static void LogHistory(ILogger logger) 
        {
            foreach (BoardItem item in Items)
            {
                logger.Log(item.ViewHistory());
            }
        }
}
```

### 4. FileLogger class
#### Description

Relying on an abstraction gives us freedom to use multiple implementations without breaking our code. Let's introduce a second class that implements `ILogger` but provides a different functionality. Inside the Loggers folder, create a class named `FileLogger` that implements `ILogger`. It should look something like this:
```cs
namespace Boarder.Loggers
{
    public class FileLogger : ILogger
    {
        public void Log(string value)
        {

        }
    }
}
```
We want this `FileLogger` to write data on a .txt file on our computer. It should also create a folder to log into, in case there isn't one already. You can read more about writing to a text file [here](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/file-system/how-to-write-to-a-text-file). There are a few ways to write in a text file but this one would do:
```cs
      public void Log(string value)
      {
          Directory.CreateDirectory(@"C:\BoardR");
          File.AppendAllText(@"C:\BoardR\Logs.txt", value);
      }
```
 `CreateDirectory()` creates a directory named BoardR inside the C directory in case there isn't one already, while `AppendAllText()` check if such a text file exists, creates it if it doesn't and then appends the text to a given file. Feel free to change the directory, the folder or the file name.

Alright, that's great, now let's pass this new logger to our Board class and see what happens
```cs
        static void Main(string[] args)
        {
            var tomorrow = DateTime.Now.AddDays(1);
            BoardItem task = new Task("Write unit tests", "Pesho", tomorrow);
            BoardItem issue = new Issue("Review tests", "Someone must review Pesho's tests.", tomorrow);

            Board.AddItem(task);
            Board.AddItem(issue);

            var fileLogger = new FileLogger();
            Board.LogHistory(fileLogger);
        }
```
Open a file explorer and navigate to the folder above, there should be a Logs.txt file with the written logs in it. We can now pass different logger implementations to the `Board` class without it even knowing!

### 5. LogHelper class
#### Description

Right now we must create instances of any loggers we would like to use. It would make sense if we can delegate this responsibility to a class whose only responsibility is to instantiate the correct type of logger. This will remove the need of calling `new()` inside our `Program` class.

First introduce a new enum named LoggerType inside the Loggers folder. It should have 2 values - `Console` and `File`:
```cs
    public enum LoggerType
    {
        Console,
        File
    }
```
Now we can create a static helper class called `LogHelper`. We can then introduce a `GetLogger()` method that accepts a `LoggerType` type and returns a new instance of the corresponding `Logger`.

Once you're done with the above you should be able to do the following in your `Main()`:

```cs
        static void Main(string[] args)
        {
            var tomorrow = DateTime.Now.AddDays(1);
            BoardItem task = new Task("Write unit tests", "Pesho", tomorrow);
            BoardItem issue = new Issue("Review tests", "Someone must review Pesho's tests.", tomorrow);

            Board.AddItem(task);
            Board.AddItem(issue);

            var consoleLogger = LogHelper.GetLogger(LoggerType.Console);
            Board.LogHistory(consoleLogger);

            var fileLogger = LogHelper.GetLogger(LoggerType.File);
            Board.LogHistory(fileLogger);
        }
```

Both loggers are now created by our `LogHelper` method and both should work within the same run of our application.
