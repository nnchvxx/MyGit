﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Boarder.Loggers
{
    public static class LogHelper
    {
        public static ILogger GetLogger(LoggerType logger)
        {
            if (logger == LoggerType.Console)
                return new ConsoleLogger();

                return new FileLogger();
        }
    }
}
