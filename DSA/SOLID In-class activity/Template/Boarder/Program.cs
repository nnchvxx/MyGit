﻿using Boarder.Loggers;
using Boarder.Models;
using System;

namespace Boarder
{
    class Program
    {
        static void Main(string[] args)
        {
            var tomorrow = DateTime.Now.AddDays(1);
            BoardItem task = new Task("Write unit tests", "Pesho", tomorrow);
            BoardItem issue = new Issue("Review tests", "Someone must review Pesho's tests.", tomorrow);

            Board.AddItem(task);
            Board.AddItem(issue);

            var consoleLogger = LogHelper.GetLogger(LoggerType.Console);
            Board.LogHistory(consoleLogger);

            var fileLogger = LogHelper.GetLogger(LoggerType.File);
            Board.LogHistory(fileLogger);
        }
    }
}
