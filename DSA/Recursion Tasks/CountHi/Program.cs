﻿using System;

namespace CountHi
{
    class Program
    {
        static void Main(string[] args)
        {
            var str = Console.ReadLine();
            Console.WriteLine(Count(str));
        }
        static int Count(string str)
        {
            if (str.Length<2)
            {
                return 0;
            }
            if (str.Substring(0,2).Equals("hi"))
            {
                return 1 + Count(str.Substring(1));
            }
            return Count(str.Substring(1));
        }
    }
}
