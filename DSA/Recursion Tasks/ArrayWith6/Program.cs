﻿using System;
using System.Linq;

namespace ArrayWith6
{
    class Program
    {
        static void Main(string[] args)
        {
            var arr = Console.ReadLine().Split(',').Select(x=>int.Parse(x)).ToArray();
            var index = int.Parse(Console.ReadLine());
            Console.WriteLine(FindNumber(arr,index));
        }
        static string FindNumber(int[] arr, int index)
        {
            if (index>arr.Length-1)
            {
                return "false";
            }
            if (arr[index] == 6)
            {
                return "true";
            }
            return FindNumber(arr, index + 1);
        }
    }
}
