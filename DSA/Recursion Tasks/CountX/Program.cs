﻿using System;

namespace CountX
{
    class Program
    {
        static void Main(string[] args)
        {
            var str = Console.ReadLine();
            Console.WriteLine(Count(str));
        }
        static int Count(string str)
        {
            if (str.Length==0)
            {
                return 0;
            }
            if ('x' == str[0])
            {
                return 1 + Count(str.Substring(1));
            }
            return Count(str.Substring(1));
        }
    }
}
