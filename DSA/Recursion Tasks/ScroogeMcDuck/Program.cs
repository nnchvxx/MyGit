﻿using System;
using System.Linq;

namespace ScroogeMcDuck
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = Console.ReadLine().Split().Select(int.Parse).ToArray();
            var rows = input[0];
            var cols = input[1];

            var matrix = GenerateMatrix(rows, cols);
            var start = FindIndexWithZero(matrix);
            Console.WriteLine(Solve(matrix, start.Item1, start.Item2));
        }
        static int Solve(int[,] matrix, int row, int col)
        {
            bool ended = true;
            if (row - 1 >= 0)
            {
                if ((matrix[row - 1, col] != 0))
                {
                    ended = false;
                }
            }
            if (row + 1 < matrix.GetLength(0))
            {
                if (matrix[row + 1, col] != 0)
                {
                    ended = false;
                }
            }
            if (col - 1 >= 0)
            {
                if (matrix[row, col - 1] != 0)
                {
                    ended = false;
                }
            }
            if (col + 1 < matrix.GetLength(1))
            {
                if (matrix[row , col+1] != 0)
                {
                    ended = false;
                }
            }
            if (ended)
            {
                return 0;
            }

            var (r,c) = FindMaxNumberPosition(matrix, row, col);
            matrix[r, c]--;
            return 1 + Solve(matrix, r, c);
        }
        static (int, int) FindMaxNumberPosition(int[,] matrix, int row, int col)
        {
            int tempRow = row;
            int tempCol = col;
            int maxNumber = 0;

            if (col - 1 >= 0 && matrix[row, col - 1] > maxNumber)
            {
                tempCol = col - 1;
                tempRow = row;
                maxNumber = matrix[row, col - 1];
            }
            if (col + 1 < matrix.GetLength(1) && matrix[row, col + 1] > maxNumber)
            {
                tempCol = col + 1;
                tempRow = row;
                maxNumber = matrix[row, col + 1];
            }
            if (row - 1 >= 0 && matrix[row - 1, col] > maxNumber)
            {
                tempCol = col;
                tempRow = row - 1;
                maxNumber = matrix[row - 1, col];
            }
            if (row + 1 < matrix.GetLength(0) && matrix[row + 1, col] > maxNumber)
            {
                tempRow = row + 1;
                tempCol = col;
                maxNumber = matrix[row + 1, col];
            }
            return (tempRow, tempCol);

        }
        static (int, int) FindIndexWithZero(int[,] matrix)
        {
            for (int r = 0; r < matrix.GetLength(0); r++)
            {
                for (int c = 0; c < matrix.GetLength(1); c++)
                {
                    if (matrix[r, c] == 0)
                    {
                        return (r, c);
                    }
                }
            }
            return (-1, -1);
        }


        static int[,] GenerateMatrix(int rows, int cols)
        {
            var matrix = new int[rows, cols];
            for (int r = 0; r < rows; r++)
            {
                var row = Console.ReadLine().Split();
                for (int c = 0; c < cols; c++)
                {
                    matrix[r, c] = int.Parse(row[c]);
                }
            }
            return matrix;
        }
    }
}
