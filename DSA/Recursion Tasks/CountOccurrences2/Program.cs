﻿using System;

namespace CountOccurrences2
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            Console.WriteLine(Count(n));
        }
        static int Count(int n)
        {
            if (n == 0)
            {
                return 0;
            }
            if (n%10==8)
            {
                int temp = n / 10;
                if (temp%10==8)
                {
                    return 2 + Count(n / 10);
                }
                return 1 + Count(n / 10);
            }
            return Count(n / 10);
        }
    }
}
