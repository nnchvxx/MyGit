﻿using System;

namespace BunnyEars
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = int.Parse(Console.ReadLine());
            Console.WriteLine(FindEars(input));
        }
        static int FindEars(int input)
        {
            if (input == 0)
            {
                return 0;
            }

            return 2 + FindEars(input - 1);
        }
    }
}
