﻿using System;

namespace CountOccurrences
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            Console.WriteLine(Count(n));
        }
        static int Count(int n)
        {
            if (n==0)
            {
                return 0;
            }
            if (n%10==7)
            {
                return 1 + Count(n / 10);
            }
            return Count(n /10);
        }
    }
}
