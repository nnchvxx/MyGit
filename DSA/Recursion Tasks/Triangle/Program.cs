﻿using System;

namespace Triangle
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            Console.WriteLine(GetBlocks(n));
        }
        static int GetBlocks(int n)
        {
            if (n<=1)
            {
                return n;
            }
            return n+GetBlocks(n - 1);
        }
    }
}
