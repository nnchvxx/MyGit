﻿using System;

namespace SumDigits
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            Console.WriteLine(Sum(n));
        }
        static int Sum(int n)
        {
            if (n<10)
            {
                return n;
            }
            return n%10+Sum(n / 10);
        }
    }
}
