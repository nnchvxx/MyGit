﻿using System;

namespace BunnyEars2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine(FindEars(n));
        }
        static int FindEars(int n)
        {
            if (n==0)
            {
                return 0;
            }
            if (n%2==0)
            {
                return 3 + FindEars(n - 1);
            }
            return 2 + FindEars(n - 1);
        }
    }
}
