﻿using System;

namespace ChangePi
{
    class Program
    {
        static void Main(string[] args)
        {
            var str = Console.ReadLine();
            Console.WriteLine(Change(str));
        }
        static string Change(string str)
        {
            if (str.Length<=1)
            {
                return str;
            }
            if (str.Substring(0, 2).Equals("pi"))
            {
                return "3.14"+Change(str.Substring(2));
            }
            return str[0]+Change(str.Substring(1));
        }
    }
}
