﻿using System;

namespace Fibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine(Fibonacci(n));
        }
        static long[] arr = new long[100];
        static long Fibonacci(int n)
        {
            if (arr[n] == 0)
            {
                if (n <= 1)
                {
                    return arr[n] = n;
                }
                arr[n] = Fibonacci(n - 1) + Fibonacci(n - 2);
            }
            return arr[n];
        }
    }
}
