﻿using System;

namespace Factorial
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            Console.WriteLine(Factorial(n));
        }
        static int Factorial(int n)
        {
            if (n==1)
            {
                return 1;
            }
            return n * Factorial((n-1));
        }
    }
}
