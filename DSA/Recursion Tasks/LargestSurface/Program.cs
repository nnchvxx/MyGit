﻿using System;
using System.Linq;

namespace LargestSurface
{
    class Program
    {
        static void Main(string[] args)
        {
            var dimensions = Console.ReadLine().Split().Select(int.Parse).ToArray();
            int rows = dimensions[0];
            int cols = dimensions[1];
            bool[,] visited = new bool[rows, cols];
            int max = 0;

            int[,] matrix = GenerateMatrix(rows, cols);

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    max = Math.Max(max, Solve(matrix, visited, r, c, matrix[r, c]));
                }
            }
            Console.WriteLine(max);
        }
        static int Solve(int[,] matrix, bool[,] visited, int row, int col, int current)
        {
            if (row < 0 || col< 0 || row>=matrix.GetLength(0) || col>=matrix.GetLength(1))
            {
                return 0;
            }
            if (matrix[row,col] != current)
            {
                return 0;
            }
            if (visited[row,col] == true)
            {
                return 0;
            }

            visited[row, col] = true;
            return 1 + Solve(matrix, visited, row - 1, col, current) //UP
                     + Solve(matrix, visited, row + 1, col, current) // DOWN
                     + Solve(matrix, visited, row, col-1, current) // LEFT 
                     + Solve(matrix, visited, row, col+1, current); // RIGHT
        }

        static int[,] GenerateMatrix(int rows, int cols)
        {
            int[,] matrix = new int[rows, cols];
            for (int r = 0; r < rows; r++)
            {
                string[] row = Console.ReadLine().Split();
                for (int c = 0; c < cols; c++)
                {
                    matrix[r, c] = int.Parse(row[c]);
                }
            }
            return matrix;
        }
    }
}
