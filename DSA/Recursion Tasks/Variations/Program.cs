﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Variations
{
    class Program
    {
        static List<string> variations = new List<string>();
        static void Main(string[] args)
        {
            int length = int.Parse(Console.ReadLine());
            var symbols = Console.ReadLine().Split().Select(x => x[0]).ToList();
            symbols.Sort();

            var first = symbols[0];
            var second = symbols[1];
            GetVariations(first, second, length, "");
            Console.WriteLine(string.Join($"{Environment.NewLine}",variations));
        }
        static void GetVariations(char first, char second, int length, string current)
        {
            if (current.Length == length)
            {
                variations.Add(current);
                return;
            }
            GetVariations(first, second, length, current + first);
            GetVariations(first, second, length, current + second);

        }

    }
}
