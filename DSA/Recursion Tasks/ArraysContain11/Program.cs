﻿using System;
using System.Linq;

namespace ArraysContain11
{
    class Program
    {
        static void Main(string[] args)
        {
            var arr = Console.ReadLine().Split(',').Select(x => int.Parse(x)).ToArray();
            var index = int.Parse(Console.ReadLine());
            Console.WriteLine(FindNumber(arr, index));
        }
        static int FindNumber(int[] arr, int index)
        {
            if (index > arr.Length - 1)
            {
                return 0;
            }
            if (arr[index] == 11)
            {
                return 1+FindNumber(arr, index + 1);
            }
            return FindNumber(arr, index + 1);
        }
    }
}
