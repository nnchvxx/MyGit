﻿using System;

namespace PowerN
{
    class Program
    {
        static void Main(string[] args)
        {
            var b = int.Parse(Console.ReadLine());
            var n = int.Parse(Console.ReadLine());
            Console.WriteLine(Power(b,n));
        }
        static int Power(int b, int n)
        {
            if (n==1)
            {
                return b;
            }
            return b*Power(b, n - 1);
        }
    }
}
