﻿using System;
using System.Linq;

namespace ArrayValuesTimes10
{
    class Program
    {
        static void Main(string[] args)
        {
            var arr = Console.ReadLine().Split(',').Select(x => int.Parse(x)).ToArray();
            var index = int.Parse(Console.ReadLine());
            Console.WriteLine(FindValue(arr,index).ToString().ToLower());
        }
        static bool FindValue(int[] arr, int index)
        {
            if (index==arr.Length-1)
            {
                return false;
            }
            if (arr[index]*10 == arr[index+1])
            {
                return true;
            }
            return FindValue(arr, index + 1);
        }
    }
}
