﻿using System;
using System.Collections;
using System.Text;

using LinearDataStructures.Common;

namespace LinearDataStructures.Extensions
{
	public static class DSAExtensions
	{
		public static bool AreListsEqual<T>(SinglyLinkedList<T> list1, SinglyLinkedList<T> list2)
		{
			var current1 = list1.Head;
			var current2 = list2.Head;

			while (current1!= null && current2 != null)
            {				

				if(current1.Value.Equals(current2.Value))
                {					
					current1 = current1.Next;
					current2 = current2.Next;
                }
                else
                {					
					return false;
                }
			}

            if (current1 == null && current2 == null)
            {
                return true;
            }

            return false;		
		}

		public static Node<T> FindMiddleNode<T>(SinglyLinkedList<T> list)
		{			
            int counter = 0;
            var current = list.Head;

            while (current != null)
            {
                counter++;
                current = current.Next;								
            }

			current = list.Head;

			if (counter > 1)
            {
				for (int i = 0; i < counter / 2; i++)
				{
					current = current.Next;
				}
			}
            
			return current;
        }

		public static SinglyLinkedList<T> MergeLists<T>(SinglyLinkedList<T> list1, SinglyLinkedList<T> list2) where T : IComparable
		{
			SinglyLinkedList<T> mergedList = new SinglyLinkedList<T>();

			var current1 = list1.Head;
			var current2 = list2.Head;

            while (current1 != null && current2 != null)
            {				
				if(current1.Value.CompareTo(current2.Value) < 1)
                {
					mergedList.AddFirst(current1.Value);
					current1 = current1.Next;
                }
				else
                {
					mergedList.AddFirst(current2.Value);
					current2 = current2.Next;
				}
            }

            while (current1 != null)
            {
				mergedList.AddFirst(current1.Value);
				current1 = current1.Next;
			}
           
			while(current2 != null)
            {
				mergedList.AddFirst(current2.Value);
				current2 = current2.Next;
            }

			var temp = mergedList.Head;
			SinglyLinkedList<T> result = new SinglyLinkedList<T>();

            while (temp != null)
            {
				result.AddFirst(temp.Value);
				temp = temp.Next;
            }

			return result;
		}

		public static SinglyLinkedList<T> ReverseList<T>(SinglyLinkedList<T> list)
		{
			SinglyLinkedList<T> reversedList = new SinglyLinkedList<T>();

			var current1 = list.Head;

            while (current1 != null)
            {
				reversedList.AddFirst(current1.Value);
				current1 = current1.Next;
            }

			return reversedList;

		}

		public static bool AreValidParentheses(string expression)
		{
			Stack st = new Stack();

			for (int i = 0; i < expression.Length; i++)
			{
				if (expression[i] == '(')
				{
					st.Push(expression[i]);
				}
				else if (expression[i] == ')')
                {
					if(st.Count == 0)
                    {
						return false;
                    }

					st.Pop();
                }
            }

			if(st.Count == 0)
            {
				return true;
            }
			return false;
		}

		public static string RemoveBackspaces(string sequence, char backspaceChar)
		{
			Stack st = new Stack();
			
            for (int i = 0; i < sequence.Length; i++)
            {
				if(sequence[i] != backspaceChar)
                {
					st.Push(sequence[i]);
                }
				if(sequence[i] == backspaceChar && st.Count > 0)
				{ 
					st.Pop();
                }
            }
			string result = "";

            foreach (var item in st)
            {
				result += item;
            }

			var temp = result.ToCharArray();
			Array.Reverse(temp);


			return string.Join("", temp);
				;
		}
	}
}
