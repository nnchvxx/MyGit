﻿
using LinearDataStructures.Common;
using System;

namespace LinearDataStructures.Tasks
{
    class Program
    {
        static void Main()
        {
            int[] array1 = new int[] { 1, 2, 3 };
            int[] array2 = new int[] { 1, 2, 3 };

            var list1 = new SinglyLinkedList<int>(array1);
            var list2 = new SinglyLinkedList<int>(array2);

            int counter = 0;
            var currnet = list1.Head;

            while (currnet != null)
            {
                counter++;
                currnet = currnet.Next;
            }
            Console.WriteLine(counter);
            /*bool areEqual = false;
            while (list1.Head != null)
            {
                if(list1.Head.Value == list2.Head.Value)
                {
                    areEqual = true;
                }
                else
                {
                    areEqual = false;
                    break;
                }
                list1.RemoveFirst();
                list2.RemoveFirst();
            }

            Console.WriteLine(areEqual);*/
            /*static bool AreListsEqual<T>(SinglyLinkedList<T> list1, SinglyLinkedList<T> list2)
            {
                int list1Count = 0;
                var current = new Node<T>(list1.Head.Value);
                while (current != null)
                {
                    list1Count++;
                    current = current.Next;
                }
                return false;
            }*/


        }
    }
}
