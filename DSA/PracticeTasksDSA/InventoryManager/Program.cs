﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InventoryManager
{
    public class Program
    {
        static void Main(string[] args)
        {
            var names = new HashSet<string>();
            var products = new Dictionary<string, HashSet<Item>>();
            StringBuilder sb = new StringBuilder();
            while (true)
            {
                var command = Console.ReadLine().Split(' ');
                if (command[0] == "end")
                {
                    break;
                }
                switch (command[0])
                {
                    case "add":
                        AddItem(command, products, sb, names);
                        break;
                    case "filter":
                        Filter(command, products, sb);
                        break;
                }
            }
            Console.WriteLine(sb.ToString());

        }
        static void Filter(string[] command, Dictionary<string, HashSet<Item>> products, StringBuilder sb)
        {
            var filterBy = command[2];
            var filtered = new HashSet<Item>();
            if (filterBy == "type")
            {
                string type = command[3];
                if (!products.ContainsKey(type))
                {
                    sb.AppendLine($"Error: Type {type} does not exists");
                    return;
                }
                filtered = products[type].OrderBy(p => p.Price).ThenBy(p => p.Name).ThenBy(p => p.Type).Take(10).ToHashSet();
            }
            else if (filterBy == "price")
            {
                if (command.Length > 5)
                {
                    var min = decimal.Parse(command[4]);
                    var max = decimal.Parse(command[6]);
                    filtered = products.Values.SelectMany(p => p)
                                        .Where(p => p.Price >= min && p.Price <= max)
                                        .OrderBy(p => p.Price)
                                        .ThenBy(p => p.Name)
                                        .ThenBy(p => p.Type)
                                        .Take(10)
                                        .ToHashSet();
                }
                else if (command[3] == "from")
                {
                    var min = decimal.Parse(command[4]);
                    filtered = products.Values.SelectMany(p => p)
                                        .Where(p => p.Price >= min)
                                        .OrderBy(p => p.Price)
                                        .ThenBy(p => p.Name)
                                        .ThenBy(p => p.Type)
                                        .Take(10)
                                        .ToHashSet();


                }
                else if (command[3] == "to")
                {

                    var max = decimal.Parse(command[4]);
                    filtered = products.Values.SelectMany(p => p)
                                        .Where(p => p.Price <= max)
                                        .OrderBy(p => p.Price)
                                        .ThenBy(p => p.Name)
                                        .ThenBy(p => p.Type)
                                        .Take(10)
                                        .ToHashSet();
                }
            }
            if (filtered.Count == 0)
            {
                sb.AppendLine("Ok: ");
                return;
            }
            string output = string.Join(", ", filtered);
            sb.AppendLine($"Ok: {output}");
        }
        static void AddItem(string[] commands, Dictionary<string, HashSet<Item>> products, StringBuilder sb, HashSet<string> names)
        {
            var name = commands[1];
            bool existingItem = names.Add(name);
            if (!existingItem)
            {
                sb.AppendLine($"Error: Item {name} already exists");
                return;
            }
            var price = decimal.Parse(commands[2]);
            var type = commands[3];
            var item = new Item(name, price, type);
            if (!products.ContainsKey(type))
            {
                products.Add(type, new HashSet<Item>());
            }
            products[type].Add(item);
            sb.AppendLine($"Ok: Item {name} added successfully");
        }

    }
    public class Item
    {
        public Item(string name, decimal price, string type)
        {
            this.Name = name;
            this.Price = price;
            this.Type = type;
        }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Type { get; set; }
        public override string ToString()
        {
            return $"{this.Name}({this.Price:0.####})";
        }
    }
}


