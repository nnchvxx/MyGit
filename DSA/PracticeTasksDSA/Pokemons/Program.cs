﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pokemons
{
    public class Program
    {
        static void Main(string[] args)
        {
            var typeMap = new Dictionary<string, HashSet<Pokemon>>();
            var pokemons = new List<Pokemon>();
            StringBuilder sb = new StringBuilder();
            while (true)
            {
                var input = Console.ReadLine();
                if (input == "end")
                {
                    break;
                }
                var commands = input.Split();
                switch (commands[0])
                {
                    case "add":
                        AddPokemon(commands, typeMap, pokemons, sb);
                        break;
                    case "find":
                        FindType(commands[1], typeMap, sb);
                        break;
                    case "ranklist":
                        Rank(commands, pokemons, sb);
                        break;
                }
            }
            Console.WriteLine(sb.ToString());

        }
        static void Rank(string[] commands, List<Pokemon> pokemons, StringBuilder sb)
        {
            var start = int.Parse(commands[1])-1;  
            var end = int.Parse(commands[2]);
            string result = string.Join("; ",  pokemons.Select(p=>$"{pokemons.IndexOf(p)+1}. {p}")
                                                       .Skip(start).Take(end - start));
            sb.AppendLine(result);
        }
        static void FindType(string type, Dictionary<string, HashSet<Pokemon>> typeMap, StringBuilder sb)
        {
            if (!typeMap.ContainsKey(type))
            {
                sb.AppendLine($"Type {type}: ");
                return;
            }
            sb.Append($"Type {type}: ");
            string result = string.Join("; ", typeMap[type].OrderBy(p=>p.Name).ThenByDescending(p=>p.Power).Take(5));
            sb.AppendLine(result);
        }
        static void AddPokemon(string[] commands, Dictionary<string, HashSet<Pokemon>> typeMap, List<Pokemon> pokemons, StringBuilder sb)
        {
            var name = commands[1];
            var type = commands[2];
            var power = int.Parse(commands[3]);
            var position = int.Parse(commands[4]);
            var pokemon = new Pokemon(name, type, power);
            pokemons.Insert(position-1, pokemon);
            if (!typeMap.ContainsKey(type))
            {
                typeMap.Add(type, new HashSet<Pokemon>());
            }
            typeMap[type].Add(pokemon);
            sb.AppendLine($"Added pokemon {name} to position {position}");
        }
    }
    public class Pokemon
    {
        public Pokemon(string name, string type, int power)
        {
            this.Name = name;
            this.Type = type;
            this.Power = power;
        }
        public string Name { get; set; }
        public string Type { get; set; }
        public int Power { get; set; }
        public override string ToString()
        {
            return $"{this.Name}({this.Power})";
        }
    }
}
