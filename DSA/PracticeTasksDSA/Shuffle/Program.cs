﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Shuffle
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = Console.ReadLine().Split().Select(int.Parse).ToArray();
            var numbers = Console.ReadLine().Split().Select(int.Parse).ToArray();

            var map = new Dictionary<int, LinkedListNode<int>>();
            var list = new LinkedList<int>();

            for (int i = 1; i <= input[0]; i++)
            {
                map.Add(i, list.AddLast(i));
            }
            var afterNumber = 0;
            for (int i = 0; i < input[1]; i++)
            {
                var numberToMove = numbers[i];
                if (numberToMove % 2 == 0)
                {
                    afterNumber = numberToMove / 2;
                }
                else
                {
                    afterNumber = numberToMove * 2;
                    if (afterNumber>input[0])
                    {
                        afterNumber = input[0];
                    }
                    if (afterNumber==numberToMove)
                    {
                        continue;
                    }
                }
                list.Remove(map[numbers[i]]);
                list.AddAfter(map[afterNumber], map[numbers[i]]);
            }
            Console.WriteLine(string.Join(" ", list));
        }
    }
}
