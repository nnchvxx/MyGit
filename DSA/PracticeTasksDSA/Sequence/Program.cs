﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sequence
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = Console.ReadLine().Split().Select(int.Parse).ToArray();
            var k = input[0];
            var n = input[1];

            var queue = new Queue<int>();
            var list = new List<int>();

            queue.Enqueue(k);
            list.Add(k);

            while (list.Count<=n)
            {
                var temp = queue.Dequeue();
                queue.Enqueue(temp + 1);
                queue.Enqueue(2 * temp + 1);
                queue.Enqueue(temp + 2);

                list.Add(temp + 1);
                list.Add(2*temp + 1);
                list.Add(temp + 2);
            }
            Console.WriteLine(list[n-1]);
        }

    }
}
