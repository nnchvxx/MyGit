﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PracticeTasksDSA
{
    class Program
    {
        static void Main(string[] args)
        {

            int n = int.Parse(Console.ReadLine());
            string[] numbersStr = Console.ReadLine().Split();
            var numbers = new int[n];
            int maxJumps = 0;
            int maxNumber = int.MinValue;
            var jumps = new List<int>();

            for (int i = n - 1; i >= 0; i--)
            {
                int currentJumps = 0;
                numbers[i] = int.Parse(numbersStr[i]);
                int currentNumber = numbers[i];
                if (currentNumber > maxNumber) maxNumber = currentNumber;
                for (int j = i + 1; j < n; j++)
                {

                    if (numbers[j] > currentNumber)
                    {
                        currentNumber = numbers[j];
                        currentJumps++;
                        if (numbers[j] == maxNumber)
                        {
                            break;
                        }
                    }
                }
                if (currentJumps > maxJumps)
                {
                    maxJumps = currentJumps;
                }
                jumps.Add(currentJumps);
            }
            jumps.Reverse();
            Console.WriteLine(maxJumps);
            Console.WriteLine(string.Join(" ",jumps));

        }
    }
}