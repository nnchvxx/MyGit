﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SmallWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            var dimensions = Console.ReadLine().Split().Select(int.Parse).ToArray();
            int rows = dimensions[0];
            int cols = dimensions[1];
            int[,] matrix = GenerateMatrix(rows, cols);
            var list = new List<int>();
            var visisted = new bool[rows, cols];

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    var output = SolveMatrix(matrix, visisted, r, c, matrix[r, c]);
                    if (output!=0)
                    {
                        list.Add(output);
                    }
                }
            }
            list = list.OrderByDescending(l => l).ToList();
            Console.WriteLine(string.Join(Environment.NewLine, list));
        }
        static int SolveMatrix(int[,] matrix, bool[,] visited, int row, int col, int current)
        {
            if (row < 0 || col < 0 || col >= matrix.GetLength(1) || row >= matrix.GetLength(0))
            {
                return 0;
            }
            if (matrix[row, col] == '0')
            {
                return 0;
            }
            if (visited[row, col] == true)
            {
                return 0;
            }
            visited[row, col] = true;

            return 1 + SolveMatrix(matrix, visited, row - 1, col, current)
                     + SolveMatrix(matrix, visited, row, col - 1, current)
                     + SolveMatrix(matrix, visited, row + 1, col, current)
                     + SolveMatrix(matrix, visited, row, col + 1, current);
        }

        private static int[,] GenerateMatrix(int rows, int cols)
        {
            int[,] matrix = new int[rows, cols];
            for (int r = 0; r < rows; r++)
            {
                var row = Console.ReadLine();
                for (int c = 0; c < cols; c++)
                {
                    matrix[r, c] = row[c];
                }
            }
            return matrix;
        }
    }
}
