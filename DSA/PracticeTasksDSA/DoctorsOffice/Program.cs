﻿using Magnum.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DoctorsOffice
{
    class Program
    {
        static void Main(string[] args)
        {
            var names = new Dictionary<string, int>();
            //var positions = new Dictionary<int, LinkedListNode<string>>();
            var queue = new BigList<string>();

            var sb = new StringBuilder();
            while (true)
            {
                var input = Console.ReadLine();
                if (input == "End")
                {
                    break;
                }
                var commands = input.Split();
                switch (commands[0])
                {
                    case "Append":
                        Append(sb, commands[1], names, queue);
                        break;
                    case "Insert":
                        Insert(sb, commands[2], int.Parse(commands[1]), queue, names);
                        break;
                    case "Find":
                        Find(sb, commands[1], names);
                        break;
                    case "Examine":
                        Examine(sb, int.Parse(commands[1]), queue, names);
                        break;
                }
            }
            Console.WriteLine(sb.ToString());

        }
        static void Append(StringBuilder sb, string name, Dictionary<string, int> names, BigList<string> queue)
        {
            if (!names.ContainsKey(name))
            {
                names.Add(name, 0);
            }
            names[name]++;
            queue.Add(name);
            sb.AppendLine("OK");
        }
        static void Insert(StringBuilder sb, string name, int position, BigList<string> queue, Dictionary<string, int> names)
        {

            if (position > queue.Count)
            {
                sb.AppendLine("Error");
                return;
            }
            queue.Insert(position, name);
            if (!names.ContainsKey(name))
            {
                names.Add(name, 0);
            }
            names[name]++;
            sb.AppendLine("OK");
        }
        static void Find(StringBuilder sb, string name, Dictionary<string, int> names)
        {
            if (!names.ContainsKey(name))
            {
                sb.AppendLine("0"); return;
            }
            sb.AppendLine(names[name].ToString());
        }
        static void Examine(StringBuilder sb, int count, BigList<string> queue, Dictionary<string,int> names)
        {
            if (count > queue.Count)
            {
                sb.AppendLine("Error");
                return;
            }
            sb.AppendLine(string.Join(" ", queue.Take(count)));
            for (int i = 0; i < count; i++)
            {
                names[queue[0]]--;
                queue.RemoveAt(0);
            }
            
        }
    }
}
