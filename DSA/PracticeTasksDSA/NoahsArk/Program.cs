﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NoahsArk
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            var animalMap = new SortedDictionary<string, int>();

            for (int i = 0; i < n; i++)
            {
                var animal = Console.ReadLine();
                if (animalMap.ContainsKey(animal))
                {
                    animalMap[animal]++;
                }
                else
                {
                    animalMap.Add(animal, 1);
                }
            }
            foreach (var animal in animalMap)
            {
                if (animal.Value % 2 == 0)
                {
                    Console.WriteLine($"{animal.Key} {animal.Value} Yes");
                }
                else
                {
                    Console.WriteLine($"{animal.Key} {animal.Value} No");
                }
            }
        }
    }
}
