﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ArmyLunch
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            var soldiers = Console.ReadLine().Split();
            var sergeants = new List<string>();
            var corporals = new List<string>();
            var privates = new List<string>();

            for (int i = 0; i < n; i++)
            {
                if (soldiers[i].StartsWith("S"))
                {
                    sergeants.Add(soldiers[i]);
                }
                if (soldiers[i].StartsWith("C"))
                {
                    corporals.Add(soldiers[i]);
                }
                if (soldiers[i].StartsWith("P"))
                {
                    privates.Add(soldiers[i]);
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Join(" ", sergeants));
            sb.Append(" ");
            sb.Append(string.Join(" ", corporals));
            sb.Append(" ");
            sb.Append(string.Join(" ", privates));
            Console.WriteLine(sb.ToString());

        }
    }
}
