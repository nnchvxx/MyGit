﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HDNLToy
{
    class Program
    {
        static void Main(string[] args)
        {
            int input = int.Parse(Console.ReadLine());

            var numbers = new Stack<int>();
            var chars = new Stack<char>();
            StringBuilder output = new StringBuilder(input * 2);
            for (int i = 0; i < input; i++)
            {
                string currentHDNL = Console.ReadLine();
                char currentChar = currentHDNL[0];
                int currentInt = int.Parse(currentHDNL.TrimStart(currentChar));
                if (numbers.Count == 0)
                {
                    numbers.Push(currentInt);
                    chars.Push(currentChar);
                    output.AppendLine($"<{currentChar}{currentInt}>");
                }
                else
                {
                    if (numbers.Peek() >= currentInt)
                    {
                        while (numbers.Count > 0 && numbers.Peek() >= currentInt)
                        {

                            output.Append(' ', numbers.Count - 1);
                            output.AppendLine($"</{chars.Pop()}{numbers.Pop()}>");
                        }
                        output.Append(' ', numbers.Count);
                        output.AppendLine($"<{currentChar}{currentInt}>");
                        numbers.Push(currentInt);
                        chars.Push(currentChar);
                    }
                    else
                    {
                        output.Append(' ', numbers.Count);
                        output.AppendLine($"<{currentChar}{currentInt}>");
                        numbers.Push(currentInt);
                        chars.Push(currentChar);
                    }
                }
            }
            while (numbers.Count > 0)
            {
                output.Append(' ', numbers.Count - 1);
                output.AppendLine($"</{chars.Pop()}{numbers.Pop()}>");
            }
            Console.WriteLine(output.ToString());
        }
    }
}
