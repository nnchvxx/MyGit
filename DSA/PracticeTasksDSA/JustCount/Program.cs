﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JustCount
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = Console.ReadLine();
            var map = new Dictionary<char, int>();
            var smallL = new Dictionary<char, int>();
            var capitalL = new Dictionary<char, int>();
            var symbols = new Dictionary<char, int>();

            for (int i = 0; i < input.Length; i++)
            {
                if (!map.ContainsKey(input[i]))
                {
                    map.Add(input[i], 0);
                }
                map[input[i]]++;
            }
            foreach (var item in map)
            {
                if (item.Key >= 65 && item.Key <= 90)
                {
                    capitalL.Add(item.Key, item.Value);
                }
                else if (item.Key >= 97 && item.Key <= 122)
                {
                    smallL.Add(item.Key, item.Value);
                }
                else
                {
                    symbols.Add(item.Key, item.Value);
                }
            }

            if (symbols.Count != 0)
            {
                var keySymbols = FindMaxChar(symbols);
                Console.WriteLine($"{keySymbols} {symbols[keySymbols]}");
            }
            else Console.WriteLine("-");
            if (smallL.Count != 0)
            {
                var keySmalls = FindMaxChar(smallL);
                Console.WriteLine($"{keySmalls} {smallL[keySmalls]}");
            }
            else Console.WriteLine("-");
            if (capitalL.Count != 0)
            {
                var keyCapitals = FindMaxChar(capitalL);
                Console.WriteLine($"{keyCapitals} {capitalL[keyCapitals]}");
            }
            else Console.WriteLine("-");

        }
        static char FindMaxChar(Dictionary<char, int> map)
        {
            int max = -1;
            char c = '\0';
            foreach (var item in map)
            {
                if (item.Value > max)                       
                {
                    max = item.Value;
                    c = item.Key;
                }
                else if (item.Value == max)
                {
                    if (c>item.Key)
                    {
                        c = item.Key;
                    }
                }
            }
            return c;
        }
    }
}
