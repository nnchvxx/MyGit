﻿using System;
using System.Collections.Generic;

namespace StudentsOrder
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = Console.ReadLine().Split();
            var k = int.Parse(input[1]);
            var names = new LinkedList<string>(Console.ReadLine().Split());
            var studentMap = new Dictionary<string, LinkedListNode<string>>();
            var node = names.First;
            foreach (var name in names)
            {
                studentMap.Add(name, node);
                node = node.Next;
            }

            for (int i = 0; i < k; i++)
            {
                var pair = Console.ReadLine().Split();
                var moveStudent = studentMap[pair[0]];
                var targetStudent = studentMap[pair[1]];

                names.Remove(moveStudent);
                names.AddBefore(targetStudent,moveStudent);
                
            }
            Console.WriteLine(string.Join(" ", names));
        }
    }
}