﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Academy.Commands.Creating;
using Academy.Core.Contracts;
using Academy.Models.Contracts;
using Academy.Models.Utils.Contracts;
using Academy.Models.Utils.LectureResources;
using Academy.Core.Factories;

namespace Academy.Tests.CommandsTests.AcademyFactoryTests.AcademyFactory
{
    [TestClass]
    public class CreateLectureResourse_Should
    {
        [TestMethod]
        public void AcademyFactory_CreateLectureResourse_ReturnVideoResourse()
        {
            var videoResourse = Core.Factories.AcademyFactory.Instance.CreateLectureResource("video", "Kiril", "BoqnURL");

            Assert.IsInstanceOfType(videoResourse, typeof(VideoResource));
        }

        [TestMethod]
        public void AcademyFactory_CreateLectureResourse_ReturnPresentation()
        {
            var presentation = Core.Factories.AcademyFactory.Instance.CreateLectureResource("presentation", "Kiril", "BoqnURL");

            Assert.IsInstanceOfType(presentation, typeof(PresentationResource));
        }

        [TestMethod]
        public void AcademyFactory_CreateLectureResourse_ReturnDemo()
        {
            var demo = Core.Factories.AcademyFactory.Instance.CreateLectureResource("demo", "Kiril", "BoqnURL");

            Assert.IsInstanceOfType(demo, typeof(DemoResource));
        }

        [TestMethod]
        public void AcademyFactory_CreateLectureResourse_ReturnHomeWork()
        {
            var homework = Core.Factories.AcademyFactory.Instance.CreateLectureResource("homework", "Kiril", "BoqnURL");

            Assert.IsInstanceOfType(homework, typeof(HomeworkResource));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void AcademyFactory_CreateLectureResourse_ReturnDefaultExeption()
        {
            var homework = Core.Factories.AcademyFactory.Instance.CreateLectureResource("Radko", "Kiril", "BoqnURL");
        }
    }
}
