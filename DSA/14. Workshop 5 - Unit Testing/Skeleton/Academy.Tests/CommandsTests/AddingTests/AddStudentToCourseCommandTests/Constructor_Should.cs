﻿using Academy.Commands.Adding;
using Academy.Core.Contracts;
using Academy.Models.Contracts;
using Academy.Models.Utils.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Academy.Tests.CommandsTests.AddingTests.AddStudentToCourseCommandTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ThrowWhenFactoryIsNull()
        {
            FakeFactory factory = null;
            FakeEngine engine = new FakeEngine();

            var addStudent = new AddStudentToCourseCommand(factory, engine);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ThrowWhenEngineIsNull()
        {
            FakeFactory factory = new FakeFactory();
            FakeEngine engine = null;

            var addStudent = new AddStudentToCourseCommand(factory, engine);
        }

        [TestMethod]
        public void WhenPassedProvidersAreValid()
        {
            FakeFactory factory = new FakeFactory();
            FakeEngine engine = new FakeEngine();

            var addStudent = new AddStudentToCourseCommand(factory, engine);
        }

        private class FakeFactory : IAcademyFactory
        {
            public ICourse CreateCourse(string name, string lecturesPerWeek, string startingDate)
            {
                throw new NotImplementedException();
            }

            public ICourseResult CreateCourseResult(ICourse course, string examPoints, string coursePoints)
            {
                throw new NotImplementedException();
            }

            public ILecture CreateLecture(string name, string date, ITrainer trainer)
            {
                throw new NotImplementedException();
            }

            public ILectureResource CreateLectureResource(string type, string name, string url)
            {
                throw new NotImplementedException();
            }

            public ISeason CreateSeason(string startingYear, string endingYear, string initiative)
            {
                throw new NotImplementedException();
            }

            public IStudent CreateStudent(string username, string track)
            {
                throw new NotImplementedException();
            }

            public ITrainer CreateTrainer(string username, string technologies)
            {
                throw new NotImplementedException();
            }
        }

        private class FakeEngine : IEngine
        {
            public IReader Reader { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public IWriter Writer { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public IParser Parser { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

            public IList<ISeason> Seasons => throw new NotImplementedException();

            public IList<IStudent> Students => throw new NotImplementedException();

            public IList<ITrainer> Trainers => throw new NotImplementedException();

            public void Start()
            {
                throw new NotImplementedException();
            }
        }

    }
}
