﻿using Academy.Models;
using Academy.Models.Contracts;
using Academy.Models.Enums;
using Academy.Models.Utils.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Academy.Tests.ModelsTests.UserTests
{
    [TestClass]

    public class Constructor_Should
    {
        [TestMethod]

        public void UsernameProperty_Name_Initialize_Properly()
        {
            //Arrange
            string name = "Ivan";
            
            //Act
            var student = new Student(name, Track.Dev);

            //Assert
            Assert.AreEqual(name, student.Username);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void UsernameProperty_Name_IsLessThanMinLength()
        {
            string name = new string('a', 2);

            //Act
            var student = new Student(name, Track.Dev);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void UsernameProperty_Name_IsMoreThanMaxLength()
        {
            string name = new string('a', 17);

            //Act
            var student = new Student(name, Track.Dev);
        }
    }
}
