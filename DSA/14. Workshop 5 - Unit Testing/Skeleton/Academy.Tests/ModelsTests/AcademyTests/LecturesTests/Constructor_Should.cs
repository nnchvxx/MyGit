﻿using Academy.Models;
using Academy.Models.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Academy.Tests.ModelsTests.AcademyTests.LecturesTests
{
    [TestClass]

    public class Constructor_Should
    {
        [TestMethod]
        public void Constructor_Should_Correctly_Initialize_Collections()
        {
            //Arrange
            string name = "LectureName";
            DateTime date = DateTime.Now;
            ITrainer radko = new FakeTrainer();

            //Act
            var lecture = new Lecture(name, date, radko);

            //Assert
            Assert.AreEqual(lecture.Resources.Count, 0);
        }

        [TestMethod]
        public void Constructor_Should_Lecture_Initialize_Properly_Name()
        {
            //Arrange
            string name = "LectureName";
            DateTime date = DateTime.Now;
            ITrainer radko = new FakeTrainer();

            //Act
            var lecture = new Lecture(name, date, radko);

            Assert.AreEqual(name, lecture.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_Should_Lecture_ThrowWhenLectureNameIsNull()
        {
            //Arrange
            string name = null;
            DateTime date = DateTime.Now;
            ITrainer radko = new FakeTrainer();

            //Act
            var lecture = new Lecture(name, date, radko);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_Should_LecturesPerWeek_ThrowWhenLecturesIsLessThanMinLength()
        {
            //Arrange
            string name = new string('a', 4);
            DateTime date = DateTime.Now;
            ITrainer radko = new FakeTrainer();

            //Act
            var lecture = new Lecture(name, date, radko);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_Should_LecturesPerWeek_ThrowWhenLecturesIsMoreThanMaxLength()
        {
            //Arrange
            string name = new string('a', 31);
            DateTime date = DateTime.Now;
            ITrainer radko = new FakeTrainer();

            //Act
            var lecture = new Lecture(name, date, radko);
        }

        private class FakeTrainer : ITrainer
        {
            public IList<string> Technologies { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public string Username { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        }
    }
}
