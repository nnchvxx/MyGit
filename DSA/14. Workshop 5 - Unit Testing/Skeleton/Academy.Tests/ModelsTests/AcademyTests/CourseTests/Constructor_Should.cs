﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using Academy.Models;

namespace Academy.Tests.ModelsTests.AcademyTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]

        public void Constructor_Should_Correctly_Initialize_Collections()
        {
            //Arrange
            string name = "Ivan";
            int lecturesPerWeek = 2;
            DateTime? staringDate = DateTime.Now;
            DateTime? endingDate = DateTime.Now.AddDays(50);

            //Act
            var course = new Course(name, lecturesPerWeek, staringDate, endingDate);


            //Assert
            Assert.AreEqual(course.OnlineStudents.Count, 0);
            Assert.AreEqual(course.OnlineStudents.Count, 0);
            Assert.AreEqual(course.Lectures.Count, 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_Should_ThrowWhenNameIsNull()
        {
            //Arrange
            string name = null;
            int lecturesPerWeek = 2;
            DateTime? staringDate = DateTime.Now;
            DateTime? endingDate = DateTime.Now.AddDays(50);

            //Act
            var course = new Course(name, lecturesPerWeek, staringDate, endingDate);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_Should_ThrowWhenNameIsLessThanMinLength()
        {
            //Arrange
            string name = new string ('a', 1);
            int lecturesPerWeek = 2;
            DateTime? staringDate = DateTime.Now;
            DateTime? endingDate = DateTime.Now.AddDays(50);

            //Act
            var course = new Course(name, lecturesPerWeek, staringDate, endingDate);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_Should_ThrowWhenNameIsMoreThanMaxLength()
        {
            //Arrange
            string name = new string('a', 46);
            int lecturesPerWeek = 2;
            DateTime? staringDate = DateTime.Now;
            DateTime? endingDate = DateTime.Now.AddDays(50);

            //Act
            var course = new Course(name, lecturesPerWeek, staringDate, endingDate);
        }

        [TestMethod]
        public void Constructor_Should_LecturesPerWeek_Initialize_Properly()
        {
            //Arrange
            string name = "Ivan";
            int lecturesPerWeek = 7;
            DateTime? staringDate = DateTime.Now;
            DateTime? endingDate = DateTime.Now.AddDays(50);

            //Act
            var course = new Course(name, lecturesPerWeek, staringDate, endingDate);

            //Assert
            Assert.AreEqual(lecturesPerWeek, course.LecturesPerWeek);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_Should_LecturesPerWeek_ThrowWhenLecturesIsLessThanMinLength()
        {
            //Arrange
            string name = "Ivan";
            int lecturesPerWeek = 0;
            DateTime? staringDate = DateTime.Now;
            DateTime? endingDate = DateTime.Now.AddDays(50);

            //Act
            var course = new Course(name, lecturesPerWeek, staringDate, endingDate);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_Should_LecturesPerWeek_ThrowWhenLecturesIsMoreThanMaxLength()
        {
            //Arrange
            string name = "Ivan";
            int lecturesPerWeek = 8;
            DateTime? staringDate = DateTime.Now;
            DateTime? endingDate = DateTime.Now.AddDays(50);

            //Act
            var course = new Course(name, lecturesPerWeek, staringDate, endingDate);
        }

        [TestMethod]
        public void Constructor_Should_StartingDate_Initialize_Properly()
        {
            //Arrange
            string name = "Petar";
            int lecturesPerWeek = 3;
            DateTime? staringDate = DateTime.Now;
            DateTime? endingDate = DateTime.Now.AddDays(50);

            //Act
            var course = new Course(name, lecturesPerWeek, staringDate, endingDate);

            //Assert
            Assert.AreEqual(staringDate, course.StartingDate);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_Should_ThrowWhenStartingDateIsNull()
        {
            //Arrange
            string name = "Petar";
            int lecturesPerWeek = 4;
            DateTime? staringDate = null;
            DateTime? endingDate = DateTime.Now.AddDays(50);

            //Act
            var course = new Course(name, lecturesPerWeek, staringDate, endingDate);
        }

        [TestMethod]
        public void Constructor_Should_EndingDate_Initialize_Properly()
        {
            //Arrange
            string name = "Ivan";
            int lecturesPerWeek = 3;
            DateTime? staringDate = DateTime.Now;
            DateTime? endingDate = DateTime.Now.AddDays(50);

            //Act
            var course = new Course(name, lecturesPerWeek, staringDate, endingDate);

            //Assert
            Assert.AreEqual(endingDate, course.EndingDate);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_Should_ThrowWhenEndingDateIsNull()
        {
            //Arrange
            string name = "asdd";
            int lecturesPerWeek = 4;
            DateTime? staringDate = DateTime.Now;
            DateTime? endingDate = null;

            //Act
            var course = new Course(name, lecturesPerWeek, staringDate, endingDate);
        }
    }
}
