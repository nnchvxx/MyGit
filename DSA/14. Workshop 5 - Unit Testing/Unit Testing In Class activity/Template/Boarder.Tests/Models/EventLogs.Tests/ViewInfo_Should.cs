﻿using Boarder.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Boarder.Tests.EventLogs.Tests
{
    [TestClass]
    public class ViewInfo_Should
    {
        [TestMethod]
        public void ViewInfoIssue()
        {
            var title = "This is a test title";
            var description = "TestUser";
            var dueDate = Convert.ToDateTime("01-01-2030");

            var issue = new Issue(title, description, dueDate);

            Assert.AreEqual($"Issue: 'This is a test title', [Open|01-01-2030] Description: TestUser", issue.ViewInfo());
        }

        [TestMethod]
        public void ViewInfoTask()
        {
            var title = "This is a test title";
            var assignee = "TestUser";
            var dueDate = Convert.ToDateTime("01-01-2030");

            var task = new Task(title, assignee, dueDate);

            Assert.AreEqual($"Task: 'This is a test title', [Todo|01-01-2030] Assignee: TestUser", task.ViewInfo());
        }
    }   
}
