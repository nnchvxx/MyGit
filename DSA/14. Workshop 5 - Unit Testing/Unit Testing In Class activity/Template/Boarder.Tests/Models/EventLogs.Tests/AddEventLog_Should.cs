﻿using Boarder.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Boarder.Tests.EventLogs.Tests
{
    [TestClass]
    public class AddEventLog_Should
    {
        [TestMethod]
        public void AddEventLogIssue()
        {
            var title = "This is a test title";
            var description = "TestUser";
            var dueDate = Convert.ToDateTime("01-01-2030");

            var issue = new Issue(title, description, dueDate);

            // $"Created Issue: {this.ViewInfo()}. Description: {this.Description}"
            Assert.AreEqual($"[{DateTime.Now:yyyyMMdd|HH:mm:ss.ffff}]Created Issue: {issue.ViewInfo()}", issue.ViewHistory());

            // Assert.AreEqual failed. Expected:< 
            // [20210215|14:34:03.2910]Created Issue: Issue: 'This is a test title', [Open|01-01-2030] Description: TestUser. 
        //     [20210215|14:34:03.2910]Created Issue: Issue: 'This is a test title', [Open|01-01-2030] Description: TestUser. Description: TestUser>. 
        }
    }
}
