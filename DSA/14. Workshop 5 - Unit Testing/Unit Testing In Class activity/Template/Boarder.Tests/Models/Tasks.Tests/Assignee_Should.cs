﻿using Boarder.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Boarder.Tests.Models.Tasks.Tests
{
    [TestClass]
    public class Assignee_Should
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Please provide a non-null or empty value")]
        public void AssigneeNullException()
        {
            // Arange
            var title = "OneTwo";
            string assignee = null;
            var dueDate = Convert.ToDateTime("01-01-2030");

            //Act
            var sut = new Task(title, assignee, dueDate);

            //Assert
            Assert.AreEqual(assignee, sut.Assignee);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Please provide a value in range [5..30]")]
        public void AssigneeMinLengthException()
        {
            // Arange
            var title = "OneTwo";
            var assignee = "One";
            var dueDate = Convert.ToDateTime("01-01-2030");

            //Act
            var sut = new Task(title, assignee, dueDate);

            //Assert
            Assert.AreEqual(assignee, sut.Assignee);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Please provide a value in range [5..30]")]
        public void AssigneeMaxLengthException()
        {
            // Arange
            var title = "OneTwo";
            var assignee = "But how when the method itself is private(thus not being able to call it from the test file)? What we can do is test it via the property itself, as this is where it's used. You can think of private methods as an add-on to the public method(in this case property). We can't test it directly but can via the provided public interface.";
            var dueDate = Convert.ToDateTime("01-01-2030");

            //Act
            var sut = new Task(title, assignee, dueDate);

            //Assert
            Assert.AreEqual(assignee, sut.Assignee);
        }
    }
}
