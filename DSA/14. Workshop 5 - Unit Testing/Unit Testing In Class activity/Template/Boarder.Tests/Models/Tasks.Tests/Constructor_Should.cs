﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Boarder.Models;

namespace Boarder.Tests.Models.Tasks.Tests
{
    [TestClass]
    public class Constructor_Should
    {

        [TestMethod]
        public void AssignCorrectValues()
        {
            //Arrange
            var title = "This is a test title";
            var assignee = "TestUser";
            var dueDate = Convert.ToDateTime("01-01-2030");
            var status = Status.Todo;

            //Act
            var sut = new Task(title, assignee, dueDate);

            //Assert
            Assert.AreEqual(title, sut.Title);
            Assert.AreEqual(assignee, sut.Assignee);
            Assert.AreEqual(dueDate, sut.DueDate);
            Assert.AreEqual(status, sut.Status);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void AssignCorrectValues_NullException()
        {
            // Arange
            string title = null;
            var assignee = "TestUser";
            var dueDate = Convert.ToDateTime("01-01-2030");

            //Act
            var sut = new Task(title, assignee, dueDate);

            //Assert
            Assert.AreEqual(title, sut.Title);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void AssignCorrectValues_MinLengthException()
        {
            // Arange
            string title = "One";
            var assignee = "TestUser";
            var dueDate = Convert.ToDateTime("01-01-2030");

            //Act
            var sut = new Task(title, assignee, dueDate);

            //Assert
            Assert.AreEqual(title, sut.Title);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void AssignCorrectValues_MaxLengthException()
        {
            // Arange
            string title = "You can leave comments for the 3A pattern to guide you through the structure of each test. Think of what we need to Arrange prior to performing the Act and what exactly we'll have to Assert at the end. In this case the Arrange part is creating the correct type which we will then pass to the GetLogger method";

            var assignee = "TestUser";
            var dueDate = Convert.ToDateTime("01-01-2030");

            //Act
            var sut = new Task(title, assignee, dueDate);

            //Assert
            Assert.AreEqual(title, sut.Title);
        }
    }
}
