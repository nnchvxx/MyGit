﻿using Boarder.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Boarder.Tests.Models.Issues.Tests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void AssignCorrectValues()
        {
            //Arrange
            var title = "This is a test title";
            var description = "TestUser";
            var dueDate = Convert.ToDateTime("01-01-2030");
            var status = Status.Open;

            //Act
            var sut = new Issue(title, description, dueDate);

            //Assert
            Assert.AreEqual(title, sut.Title);
            Assert.AreEqual(description, sut.Description);
            Assert.AreEqual(dueDate, sut.DueDate);
            Assert.AreEqual(status, sut.Status);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ValidateExceptionDueDate()
        {
            //Arrange
            var title = "This is a test title";
            var description = "TestUser";
            var dueDate = Convert.ToDateTime("01-01-2010");

            //Act
            var sut = new Issue(title, description, dueDate);

            //Assert
            Assert.AreEqual(dueDate, sut.DueDate);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ValidateExceptionDescription()
        {
            //Arrange
            var title = "This is a test title";
            string description = null;
            var dueDate = Convert.ToDateTime("01-01-2010");

            //Act
            var sut = new Issue(title, description, dueDate);

            //Assert
            Assert.AreEqual(description, sut.Description);
        }
    }
}
