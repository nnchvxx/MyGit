﻿using Boarder.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Boarder.Tests.Models.Issues.Tests
{
    [TestClass]
    public class AdvanceStatus_Should
    {

        [TestMethod]
        public void AdvanceCorrectStatus()
        {
            //Arrange
            var title = "This is a test title";
            var description = "TestUser";
            var dueDate = Convert.ToDateTime("01-01-2030");
            var issue = new Issue(title, description, dueDate);

            //Act
            issue.AdvanceStatus();

            Assert.AreEqual(issue.Status, Status.Verified);
        }

        [TestMethod]
        public void AdvanceVerifiedStatus()
        {
            //Arrange
            var title = "This is a test title";
            var description = "TestUser";
            var dueDate = Convert.ToDateTime("01-01-2030");
            var issue = new Issue(title, description, dueDate);

            //Act
            issue.AdvanceStatus();
            issue.AdvanceStatus();

            Assert.AreEqual(issue.Status, Status.Verified);
        }
    }
}
