<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

# Academy - Unit Testing Workshop

### 1. Description

You are given an already built software system. Your task is to get to know with the way the system works. Check out the tests requirements to know exactrly what you must test as not all functionalities must be tested. Before writing any code, familirize yourself with the project and go through the list of requirements. Follow the `testing guidelines` you've learned. Everything should have a `single responsibility`. Test only the things that the class is concerned about.

### 2. Project information

- Framework: **.NET Core 3.1**
- Language and version: **C# 8**

### 3. Тest the Academy.Models.Course constructor

#### Description

- It should correctly initialize the collections.

### 4. Test the Academy.Models.Course Name property

#### Description

- It should throw `ArgumentException` when passed value is invalid.
- It should correctly assign passed value.

### 5. Test the Academy.Models.Course LecturesPerWeek property

#### Description

It should throw `ArgumentException` when passed value is invalid.
It should correctly assign passed value.

### 6. Test the Academy.Models.Course StartingDate property

#### Description

- It should throw `ArgumentNullException` when passed value is invalid.
- It should correctly assign passed value.

### 7. Test the Academy.Models.Course EndingDate property

#### Description

- It should throw `ArgumentNullException` when passed value is invalid.
- It should correctly assign passed value.

### 8. Test the Academy.Models.Lecture constructor

#### Description

- It should correctly initialize the collections.

### 9. Test the Academy.Models.Lecture Name property

#### Description

- It should throw `ArgumentException` when passed value is invalid.
- It should correctly assign passed value.

### 10. Test the Academy.Models.Abstractions.User Username property

#### Description

- It should throw `ArgumentException` when passed value is invalid.
- It should correctly assign passed value.

### 11. Test the Academy.Core.Factories.AcademyFactory CreateLectureResource() method

#### Description

- It should throw `ArgumentException` when passed type is invalid.
- It should return correct type of instances.

### 12. Test the Academy.Commands.Adding.AddStudentToSeasonCommand constructor

#### Description

- It should throw `ArgumentNullException` when a passed provider is null.
- It should not throw when passed providers are valid.

### 13. Test the Academy.Commands.Adding.AddStudentToCourseCommand constructor

#### Description

- It should throw `ArgumentNullException` when a passed provider is null.
- It should not throw when passed providers are valid.

### 14. Test the Academy.Commands.Adding.AddTrainerToSeasonCommand constructor

#### Description

- It should throw `ArgumentNullException` when a passed provider is null.
- It should not throw when passed providers are valid.
