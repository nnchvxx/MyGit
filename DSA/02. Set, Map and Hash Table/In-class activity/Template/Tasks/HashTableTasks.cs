﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HashTables.InClassActivity
{
    public class HashTableTasks
    {
        /// <summary>
        /// Counts the number of occurrences of a each word in a collection.
        /// </summary>
        /// <param name="words">A collection of words.</param>
        /// <returns>A dictionary of occurrences by word.</returns>
        public static Dictionary<string, int> CountOccurences(string[] words)
        {
            var map = new Dictionary<string, int>();
            foreach (var word in words)
            {
                if (map.ContainsKey(word))
                {
                    map[word]++;
                }
                else
                {
                    map.Add(word, 1);
                }
            }
            return map;
        }

        /// <summary>
        /// Return the indices of the first two numbers that sum to a given number.
        /// </summary>
        /// <param name="numbers">Collection of numbers</param>
        /// <param name="sum">Target sum</param>
        /// <returns>An array containing the indices of the first two numbers that produce the target sum.</returns>
        public static int[] TwoSum(int[] numbers, int sum)
        {
            var map = new Dictionary<int, int>();

            for (int i = 0; i < numbers.Length; i++)
            {
                var target = sum - numbers[i];
                if (map.ContainsKey(target))
                {
                    return new int[] { map[target], i };
                }
                else
                {
                    map.Add(numbers[i], i);
                }
            }
            return new int[] { -1, -1 };
        }

        /// <summary>
        /// Counts how many coins are special.
        /// </summary>
        /// <param name="coins">Coins to check.</param>
        /// <param name="catalogue">The catalogue of special coins.</param>
        /// <returns>The count of special coins</returns>
        public static int SpecialCoins(string coins, string catalogue)
        {
            int counter = 0;
            var set = new HashSet<char>();
            set.UnionWith(catalogue);
            foreach (var coin in coins)
            {
                if (set.Contains(coin))
                {
                    counter++;
                }
            }
            return counter;
        }

        /// <summary>
        /// Determines whether two strings are isomorphic. 
        /// Two strings are considered isomorphic if each character from the first string can map to a character in the seconds string.
        /// </summary>
        /// <param name="s1">The first string.</param>
        /// <param name="s2">The second string.</param>
        /// <returns>True if the two strings are isomorphic; otherwise, false.</returns>
        public static bool AreIsomorphic(string s1, string s2)
        {
            var map = new Dictionary<char, char>();   
            for (int i = 0; i < s1.Length; i++)
            {
                var c = s1[i];
                if ((map.ContainsKey(c) && !map[c].Equals(s2[i]))
                    ||(!map.ContainsKey(c) && map.ContainsValue(s2[i])))
                {
                    return false;
                }
                map.TryAdd(c, s2[i]);
            }
            return true;
        }
    }
}
