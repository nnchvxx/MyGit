﻿using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// A Last In First Out (LIFO) collection implemented with a System.Collections.Generic.LinkedList<T>.
/// </summary>
public class Stack<T> : IEnumerable<T>
{
    private readonly LinkedList<T> list;
    public Stack()
    {
        list = new LinkedList<T>();
    }


    public void Push(T item)
    {
        list.AddFirst(item);
    }

    public T Pop()
    {
        if (list.Count == 0) throw new InvalidOperationException("Stack is empty");
        var temp = list.First;
        list.RemoveFirst();
        return temp.Value;
    }

    public T Peek()
    {
        if (list.Count == 0) throw new InvalidOperationException("Stack is empty");
        return list.First.Value;
    }

    public void Clear()
    {
        list.Clear();
    }

    public int Count
    {
        get
        {
            return list.Count;
        }
    }

    // Enumerates each item in the stack in LIFO order. 
    public IEnumerator<T> GetEnumerator()
    {
        return this.list.GetEnumerator();
    }

    // Enumerates each item in the stack in LIFO order.
    IEnumerator IEnumerable.GetEnumerator()
    {
        return this.list.GetEnumerator();
    }
}
