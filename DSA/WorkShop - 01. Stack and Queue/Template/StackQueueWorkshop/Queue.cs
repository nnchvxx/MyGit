﻿using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// A First In First Out (FIFO) collection implemented with a System.Collections.Generic.LinkedList<T>.
/// </summary>
public class Queue<T> : IEnumerable<T>
{
    private readonly LinkedList<T> list;
    public Queue()
    {
        list = new LinkedList<T>();
    }

    public void Enqueue(T item)
    {
        list.AddLast(item);
    }

    public T Dequeue()
    {
        if (list.Count == 0) throw new InvalidOperationException("Queue is empty.");
        var temp = list.First;
        list.RemoveFirst();
        return temp.Value;
    }

    public T Peek()
    {
        if (list.Count == 0) throw new InvalidOperationException("Queue is empty.");
        return list.First.Value;
    }

    public void Clear()
    {
        list.Clear();
    }

    public int Count
    {
        get
        {
            return list.Count;
        }
    }

    // Enumerates each item in the stack in FIFO order. 
    public IEnumerator<T> GetEnumerator()
    {
        return this.list.GetEnumerator();
    }

    // Enumerates each item in the stack in FIFO order. 
    IEnumerator IEnumerable.GetEnumerator()
    {
        return this.list.GetEnumerator();
    }
}