﻿using ASPNetCoreOverview.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPNetCoreOverview.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly List<User> users = new List<User>
        {
            new User
            {
                Id = 1,
                Name = "John Smith",
                Email = "j.smith@gmail.com"
            },
            new User
            {
                Id = 2,
                Name = "Chris Rock",
                Email = "c.rock@gmail.com"
            }
        };
        [HttpGet("")]
        public IActionResult Get()
        {
            return this.Ok(this.users);
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var user = this.users.FirstOrDefault(u => u.Id == id);
            if (user == null)
            {
                return this.NotFound();
            }
            return this.Ok(user);
        }
        [HttpPost("")]
        public IActionResult Post([FromBody] User user)
        {
            if (user == null)
            {
                return this.BadRequest();
            }
            this.users.Add(user);
            return this.Created("post", user);
        }
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] User model)
        {
            if (id<1 || model == null)
            {
                return this.BadRequest();
            }
            var user = this.users.FirstOrDefault(u => u.Id == id);

            user.Name = model.Name;
            user.Email = model.Email;
            return this.Ok();
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var user = this.users.FirstOrDefault(u => u.Id == id);
            if (user == null)
            {
                return this.NotFound();
            }
            this.users.Remove(user);
            return this.NoContent();

        }

    }
}
