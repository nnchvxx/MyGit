﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ASPNetCoreOverview.Models
{
    public class User
    {
        public int Id { get; set; }

        [StringLength(25,MinimumLength = 4,ErrorMessage = "Value for {0} must be between {1} and {2} characters.")]
        public string Name { get; set; }

        [StringLength(40,MinimumLength =3,ErrorMessage = "Value for {0} must be between {1} and {2} characters.")]
        public string Email { get; set; }
    }
}
