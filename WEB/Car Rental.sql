USE [master]
GO
/****** Object:  Database [Car Rental]    Script Date: 3/24/2021 11:02:35 PM ******/
CREATE DATABASE [Car Rental]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Car Rental', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Car Rental.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Car Rental_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Car Rental_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Car Rental] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Car Rental].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Car Rental] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Car Rental] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Car Rental] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Car Rental] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Car Rental] SET ARITHABORT OFF 
GO
ALTER DATABASE [Car Rental] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Car Rental] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Car Rental] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Car Rental] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Car Rental] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Car Rental] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Car Rental] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Car Rental] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Car Rental] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Car Rental] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Car Rental] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Car Rental] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Car Rental] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Car Rental] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Car Rental] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Car Rental] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Car Rental] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Car Rental] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Car Rental] SET  MULTI_USER 
GO
ALTER DATABASE [Car Rental] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Car Rental] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Car Rental] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Car Rental] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Car Rental] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Car Rental] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [Car Rental] SET QUERY_STORE = OFF
GO
USE [Car Rental]
GO
/****** Object:  Table [dbo].[Car]    Script Date: 3/24/2021 11:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Car](
	[id] [int] NOT NULL,
	[category_id] [int] NOT NULL,
	[brand] [varchar](50) NOT NULL,
	[model] [varchar](50) NOT NULL,
	[production_year] [int] NOT NULL,
	[mileage] [int] NOT NULL,
	[color] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Car] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Car_equipment]    Script Date: 3/24/2021 11:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Car_equipment](
	[id] [int] NOT NULL,
	[equipment_id] [int] NOT NULL,
	[car_id] [int] NOT NULL,
	[start_date] [date] NOT NULL,
	[end_date] [date] NOT NULL,
 CONSTRAINT [PK_Car_equipment] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 3/24/2021 11:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[id] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[City]    Script Date: 3/24/2021 11:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[City](
	[id] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 3/24/2021 11:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[id] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
	[birth_date] [date] NOT NULL,
	[driving_license_number] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Equipment]    Script Date: 3/24/2021 11:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Equipment](
	[id] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
	[equipment_category_id] [int] NOT NULL,
 CONSTRAINT [PK_Equipment] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Equipment_category]    Script Date: 3/24/2021 11:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Equipment_category](
	[id] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Equipment_category] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Insurance]    Script Date: 3/24/2021 11:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Insurance](
	[id] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
	[description] [text] NOT NULL,
 CONSTRAINT [PK_Insurance] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Location]    Script Date: 3/24/2021 11:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Location](
	[id] [int] NOT NULL,
	[city_id] [int] NOT NULL,
 CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rental]    Script Date: 3/24/2021 11:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rental](
	[id] [int] NOT NULL,
	[customer_id] [int] NOT NULL,
	[car_id] [int] NOT NULL,
	[pick_up_location_id] [int] NOT NULL,
	[drop_off_location_id] [int] NOT NULL,
	[start_date] [date] NOT NULL,
	[end_date] [date] NOT NULL,
 CONSTRAINT [PK_Rental] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rental_insurance]    Script Date: 3/24/2021 11:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rental_insurance](
	[rental_id] [int] NOT NULL,
	[insurance_id] [int] NOT NULL,
 CONSTRAINT [PK_Rental_insurance_1] PRIMARY KEY CLUSTERED 
(
	[rental_id] ASC,
	[insurance_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reservation]    Script Date: 3/24/2021 11:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reservation](
	[id] [int] NOT NULL,
	[category_id] [int] NOT NULL,
	[pick_up_location_id] [int] NOT NULL,
	[drop_off_location_id] [int] NOT NULL,
	[customer_id] [int] NOT NULL,
 CONSTRAINT [PK_Reservation] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reservation_equipment]    Script Date: 3/24/2021 11:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reservation_equipment](
	[reservation_id] [int] NOT NULL,
	[equipment_category_id] [int] NOT NULL,
 CONSTRAINT [PK_Reservation_equipment] PRIMARY KEY CLUSTERED 
(
	[reservation_id] ASC,
	[equipment_category_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Car]  WITH CHECK ADD  CONSTRAINT [FK_Car_Category] FOREIGN KEY([category_id])
REFERENCES [dbo].[Category] ([id])
GO
ALTER TABLE [dbo].[Car] CHECK CONSTRAINT [FK_Car_Category]
GO
ALTER TABLE [dbo].[Car_equipment]  WITH CHECK ADD  CONSTRAINT [FK_Car_equipment_Car] FOREIGN KEY([car_id])
REFERENCES [dbo].[Car] ([id])
GO
ALTER TABLE [dbo].[Car_equipment] CHECK CONSTRAINT [FK_Car_equipment_Car]
GO
ALTER TABLE [dbo].[Car_equipment]  WITH CHECK ADD  CONSTRAINT [FK_Car_equipment_Equipment] FOREIGN KEY([equipment_id])
REFERENCES [dbo].[Equipment] ([id])
GO
ALTER TABLE [dbo].[Car_equipment] CHECK CONSTRAINT [FK_Car_equipment_Equipment]
GO
ALTER TABLE [dbo].[Equipment]  WITH CHECK ADD  CONSTRAINT [FK_Equipment_Equipment_category] FOREIGN KEY([equipment_category_id])
REFERENCES [dbo].[Equipment_category] ([id])
GO
ALTER TABLE [dbo].[Equipment] CHECK CONSTRAINT [FK_Equipment_Equipment_category]
GO
ALTER TABLE [dbo].[Location]  WITH CHECK ADD  CONSTRAINT [FK_Location_City] FOREIGN KEY([city_id])
REFERENCES [dbo].[City] ([id])
GO
ALTER TABLE [dbo].[Location] CHECK CONSTRAINT [FK_Location_City]
GO
ALTER TABLE [dbo].[Rental]  WITH CHECK ADD  CONSTRAINT [FK_Rental_Car] FOREIGN KEY([car_id])
REFERENCES [dbo].[Car] ([id])
GO
ALTER TABLE [dbo].[Rental] CHECK CONSTRAINT [FK_Rental_Car]
GO
ALTER TABLE [dbo].[Rental]  WITH CHECK ADD  CONSTRAINT [FK_Rental_Customer] FOREIGN KEY([customer_id])
REFERENCES [dbo].[Customer] ([id])
GO
ALTER TABLE [dbo].[Rental] CHECK CONSTRAINT [FK_Rental_Customer]
GO
ALTER TABLE [dbo].[Rental]  WITH CHECK ADD  CONSTRAINT [FK_Rental_Location] FOREIGN KEY([pick_up_location_id])
REFERENCES [dbo].[Location] ([id])
GO
ALTER TABLE [dbo].[Rental] CHECK CONSTRAINT [FK_Rental_Location]
GO
ALTER TABLE [dbo].[Rental]  WITH CHECK ADD  CONSTRAINT [FK_Rental_Location1] FOREIGN KEY([drop_off_location_id])
REFERENCES [dbo].[Location] ([id])
GO
ALTER TABLE [dbo].[Rental] CHECK CONSTRAINT [FK_Rental_Location1]
GO
ALTER TABLE [dbo].[Rental_insurance]  WITH CHECK ADD  CONSTRAINT [FK_Rental_insurance_Insurance] FOREIGN KEY([insurance_id])
REFERENCES [dbo].[Insurance] ([id])
GO
ALTER TABLE [dbo].[Rental_insurance] CHECK CONSTRAINT [FK_Rental_insurance_Insurance]
GO
ALTER TABLE [dbo].[Rental_insurance]  WITH CHECK ADD  CONSTRAINT [FK_Rental_insurance_Rental] FOREIGN KEY([rental_id])
REFERENCES [dbo].[Rental] ([id])
GO
ALTER TABLE [dbo].[Rental_insurance] CHECK CONSTRAINT [FK_Rental_insurance_Rental]
GO
ALTER TABLE [dbo].[Reservation]  WITH CHECK ADD  CONSTRAINT [FK_Reservation_Category] FOREIGN KEY([category_id])
REFERENCES [dbo].[Category] ([id])
GO
ALTER TABLE [dbo].[Reservation] CHECK CONSTRAINT [FK_Reservation_Category]
GO
ALTER TABLE [dbo].[Reservation]  WITH CHECK ADD  CONSTRAINT [FK_Reservation_Customer] FOREIGN KEY([customer_id])
REFERENCES [dbo].[Customer] ([id])
GO
ALTER TABLE [dbo].[Reservation] CHECK CONSTRAINT [FK_Reservation_Customer]
GO
ALTER TABLE [dbo].[Reservation]  WITH CHECK ADD  CONSTRAINT [FK_Reservation_Location] FOREIGN KEY([pick_up_location_id])
REFERENCES [dbo].[Location] ([id])
GO
ALTER TABLE [dbo].[Reservation] CHECK CONSTRAINT [FK_Reservation_Location]
GO
ALTER TABLE [dbo].[Reservation]  WITH CHECK ADD  CONSTRAINT [FK_Reservation_Location1] FOREIGN KEY([drop_off_location_id])
REFERENCES [dbo].[Location] ([id])
GO
ALTER TABLE [dbo].[Reservation] CHECK CONSTRAINT [FK_Reservation_Location1]
GO
ALTER TABLE [dbo].[Reservation_equipment]  WITH CHECK ADD  CONSTRAINT [FK_Reservation_equipment_Equipment_category] FOREIGN KEY([equipment_category_id])
REFERENCES [dbo].[Equipment_category] ([id])
GO
ALTER TABLE [dbo].[Reservation_equipment] CHECK CONSTRAINT [FK_Reservation_equipment_Equipment_category]
GO
ALTER TABLE [dbo].[Reservation_equipment]  WITH CHECK ADD  CONSTRAINT [FK_Reservation_equipment_Reservation] FOREIGN KEY([reservation_id])
REFERENCES [dbo].[Reservation] ([id])
GO
ALTER TABLE [dbo].[Reservation_equipment] CHECK CONSTRAINT [FK_Reservation_equipment_Reservation]
GO
USE [master]
GO
ALTER DATABASE [Car Rental] SET  READ_WRITE 
GO
