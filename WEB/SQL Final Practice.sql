-- 1. Write a SQL query to find the top 5 most recently shipped orders. Display their ship name and shipped date.
SELECT TOP 5 S.ShipperName AS [Shipper Name],O.OrderDate AS [Order Date]
FROM Orders O
JOIN Shippers S ON O.ShipperID = S.ShipperID
ORDER BY O.OrderDate DESC

-- Expected Result:

-- Shipper Name         Order Date
-- -------------------------------
-- Speedy Express       1997-02-12
-- United Package       1997-02-11
-- United Package       1997-02-10
-- United Package       1997-02-10
-- Federal Shipping     1997-02-07
--------------------------------------------------------------------------------------------------
-- 2. Write a SQL query to find the top 3 (ordered alphabetically by product name) products along with their category name.
SELECT TOP 3 P.ProductName AS Product,C.CategoryName AS Category
FROM Products P
JOIN Categories C ON P.CategoryID = C.CategoryID
ORDER BY P.ProductName 

-- Expected Result:

-- Product              Category
-- ---------------------------------
-- Alice Mutton         Meat/Poultry
-- Aniseed Syrup        Condiments
-- Boston Crab Meat     Seafood
--------------------------------------------------------------------------------------------------
-- 3. Write a SQL query to find all suppliers that have more than 3 products (ordered by SupplierID). 
--    Display product's supplier company name.
SELECT S.SupplierName AS Company
FROM Suppliers S
JOIN Products P ON P.SupplierID = S.SupplierID
GROUP BY S.SupplierName, S.SupplierID
HAVING COUNT(P.ProductID)>3
ORDER BY S.SupplierID

-- Expected Result:

-- Company
-- --------------------------
-- New Orleans Cajun Delights
-- Pavlova, Ltd.
-- Specialty Biscuits, Ltd.
-- Plutzer Lebensmittelgromrkte AG
--------------------------------------------------------------------------------------------------
-- 4. Write a SQL query to find top 5 customer companies arranged by their order total. 
--    Order total is the sum of the product's quantity multiplied by its price. 
--    Display the order id, company name order total.
SELECT TOP 5 O.OrderID AS OrderID, C.CustomerName AS Company, SUM(OD.Quantity*P.Price) AS OrderTotal
FROM OrderDetails OD
JOIN Orders O ON OD.OrderID = O.OrderID
JOIN Customers C ON C.CustomerID = O.CustomerID
JOIN Products P ON OD.ProductID = P.ProductID
GROUP BY O.OrderID, C.CustomerName
ORDER BY OrderTotal DESC

-- Expected Result:

-- OrderID  Company               OrderTotal
-- -------------------------------------------
-- 10372    Queen Cozinha         15353.60
-- 10424    Mre Paillarde         14366.50
-- 10417    Simons bistro         14104.00
-- 10353    Piccolo und mehr      13427.00
-- 10360    Blondel pre et fils   9244.25
--------------------------------------------------------------------------------------------------
-- 5. Write a SQL query to display cities that have both customers and suppliers along with how many customers and how many suppliers are there.
SELECT C.City,COUNT(DISTINCT C.CustomerID) AS Customers, COUNT(DISTINCT S.SupplierID) AS Suppliers
FROM Customers C
JOIN Suppliers S ON C.City = S.City
GROUP BY C.City

-- Expected Result:

-- City         Customers   Suppliers
-- ----------------------------------
-- Berlin       1           1
-- Montral      1           1
-- Paris        2           1
-- So Paulo     4           1