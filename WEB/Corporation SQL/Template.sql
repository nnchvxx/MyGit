-- If you don't have the Corporation database, 
--    use the Corporation.sql script to create it first.
-- Then complete the tasks below. Good luck!
-----------------------------------------------------------

-- 1. Write a SQL query to find all information about all departments.
SELECT * 
FROM Departments

-- 2. Write a SQL query to find all department names.
SELECT D.Name AS DepartmentName
FROM Departments D

-- 3. Write a SQL query to find the salary of each employee.
SELECT FirstName,LastName,Salary
FROM Employees

-- 4. Write a SQL to find the full name of each employee. 
--    Full name is constructed by joining first, middle and last name.
SELECT (E.FirstName+' '+E.MiddleName+' '+E.LastName) AS 'Full Name'
FROM Employees E

-- 5. Write a SQL query to find the email addresses of each employee (by his first and last name). 
--    Consider that the mail domain is telerik.com. Emails should look like "John.Doe@telerik.com". 
--    The produced column should be named "Full Email Addresses".
SELECT (FirstName+'.'+LastName+'@telerik.com') AS 'Full Email Addresses'
FROM Employees

-- 6. Write a SQL query to find all different employee salaries.
SELECT DISTINCT Salary
FROM Employees

-- 7. Write a SQL query to find all information about the employees whose job title is "Sales Representative" or "Sales Manager".
SELECT *
FROM Employees
WHERE JobTitle='Sales representative' OR JobTitle = 'Sales Manager'

-- 8. Write a SQL query to find the names of all employees whose first name starts with "SA".
SELECT *
FROM Employees
WHERE FirstName LIKE 'SA%'

-- 9. Write a SQL query to find the names of all employees whose last name contains "ei".
SELECT * 
FROM Employees
WHERE LastName LIKE '%ei%'

-- 10. Write a SQL query to find the salary of all employees whose salary is in the range [20000…30000].
SELECT *
FROM Employees
WHERE Salary BETWEEN 20000 AND 30000

-- 11. Write a SQL query to find the names of all employees whose salary is 25000, 14000, 12500 or 23600.
SELECT *
FROM Employees
WHERE Salary IN (25000,14000,12500,23600)

-- 12. Write a SQL query to find all employees that do not have manager.
SELECT *
FROM Employees
WHERE ManagerID IS NULL 

-- 13. Write a SQL query to find all employees that have salary more than 50000. Order them in decreasing order by salary.
SELECT *
FROM Employees
WHERE Salary>50000
ORDER BY Salary DESC

-- 14. Write a SQL query to find the top 5 best paid employees.
SELECT TOP 5 *
FROM Employees
ORDER BY Salary DESC

-- 15. Write a SQL query to find all employees along with their address. Use inner join with ON clause.
SELECT E.FirstName,E.MiddleName,E.LastName,E.AddressID,A.AddressID,A.AddressText
FROM Employees E
INNER JOIN Addresses A ON E.AddressID = A.AddressID

-- 16. Write a SQL query to find all employees and their address. Use equijoins (conditions in the WHERE clause).
SELECT E.FirstName,E.MiddleName,E.LastName,E.AddressID, A.AddressID,A.AddressText
FROM Employees E, Addresses A
WHERE E.AddressID = A.AddressID

-- 17. Write a SQL query to find all employees along with their manager.
SELECT E.EmployeeID,(E.FirstName+' '+E.LastName) AS Employee, 
	   M.EmployeeID,(M.FirstName+' '+M.LastName) AS Manager
FROM Employees E, Employees M
WHERE E.ManagerID = M.EmployeeID

-- 18. Write a SQL query to find all employees, along with their manager and their address. 
--     Hint: Join Employees e, Employees m and Addresses a.
SELECT E.EmployeeID,(E.FirstName+' '+E.LastName) AS Employee, 
	   A.AddressID,A.AddressText AS Address,
	   M.EmployeeID,(M.FirstName+' '+M.LastName) AS Manager
FROM Employees E
JOIN Employees M ON E.ManagerID = M.EmployeeID
JOIN Addresses A ON E.AddressID = A.AddressID

-- 19. Write a SQL query to find all departments and all town names as a single list. Use UNION.
SELECT Name AS 'Deparment and Town Names'
FROM Departments
UNION
SELECT Name
FROM Towns

-- 20. Write a SQL query to find all the employees and the manager for each of them along with the employees that do not have manager. 
--     Use right outer join. Rewrite the query to use left outer join.
SELECT (E.FirstName+' '+E.LastName) AS Employee, 
		M.EmployeeID,(M.FirstName+' '+M.LastName) AS Manager
FROM Employees E
LEFT JOIN Employees M ON E.ManagerID = M.EmployeeID

SELECT M.EmployeeID,(M.FirstName+' '+M.LastName) AS Manager,
	   (E.FirstName+' '+E.LastName) AS Employee
FROM Employees M
RIGHT JOIN Employees E ON E.ManagerID = M.EmployeeID

-- 21. Write a SQL query to find the names of all employees from the departments "Sales" and "Finance" whose hire year is between 1995 and 2005.
SELECT CONCAT(E.FirstName,' ',E.LastName) AS Employee,
	   D.Name AS Department
FROM Employees E
JOIN Departments D ON E.DepartmentID = D.DepartmentID
WHERE D.Name IN ('Sales','Finance')
AND DATEPART(YY,E.HireDate) BETWEEN 1995 AND 2005

-- 22. Write a SQL query to find the names and salaries of the employees that take the minimal salary in the company.
--     Hint: Use a nested SELECT statement.
SELECT CONCAT(E.FirstName,' ',E.LastName) AS Employee,E.Salary 
FROM Employees E
WHERE E.Salary = (SELECT MIN(E.Salary) FROM Employees E)

-- 23. Write a SQL query to find the names and salaries of the employees that have a salary that is up to 10% higher than the minimal salary for the company.
SELECT CONCAT(E.FirstName,' ',E.LastName) AS Employee,E.Salary 
FROM Employees E
WHERE E.Salary >= (SELECT MIN(E.Salary)*1.10 FROM Employees E)

-- 24. Write a SQL query to find the full name, salary and department of the employees that take the minimal salary in their department.
--     Hint: Use a nested SELECT statement.
SELECT CONCAT(E.FirstName,' ',E.MiddleName,' ',E.LastName) AS Employee,E.Salary, D.Name AS Department
FROM Employees E
JOIN Departments D ON E.DepartmentID = D.DepartmentID
WHERE E.Salary IN (SELECT MIN(E.Salary) FROM Employees E GROUP BY E.DepartmentID) 

-- 25. Write a SQL query to find the average salary in the department with id = 1.
SELECT AVG(E.Salary),D.Name 
FROM Employees E
JOIN Departments D ON E.DepartmentID = D.DepartmentID
WHERE E.DepartmentID = 1 
GROUP BY D.Name

-- 26. Write a SQL query to find the average salary  in the "Sales" department.
SELECT AVG(E.Salary),D.Name 
FROM Employees E
JOIN Departments D ON E.DepartmentID = D.DepartmentID
WHERE D.Name = 'Sales' 
GROUP BY D.Name

-- 27. Write a SQL query to find the number of employees in the "Sales" department.
SELECT COUNT(*) 
FROM Employees E
JOIN Departments D ON E.DepartmentID = D.DepartmentID
WHERE D.Name = 'Sales'

-- 28. Write a SQL query to find the number of all employees that have manager.
SELECT COUNT(*)
FROM Employees E
WHERE E.ManagerID IS NOT NULL

-- 29. Write a SQL query to find the number of all employees that have no manager.
SELECT COUNT(*)
FROM Employees E
WHERE E.ManagerID IS NULL

-- 30. Write a SQL query to find all departments and the average salary for each of them.
SELECT D.Name AS 'Department Name',AVG(E.Salary) AS 'Average Salary'
FROM Employees E
JOIN Departments D ON E.DepartmentID = D.DepartmentID
GROUP BY D.Name

-- 31. Write a SQL query to find the count of all employees in each department and for each town.
SELECT T.Name,D.Name,COUNT(E.EmployeeID)
FROM Employees E
JOIN Departments D ON E.DepartmentID = D.DepartmentID
JOIN Addresses A ON E.AddressID = A.AddressID
JOIN Towns T ON T.TownID = A.TownID
GROUP BY T.Name,D.Name
ORDER BY T.Name

-- 32. Write a SQL query to find all managers that have exactly 5 employees. Display their first name and last name.
SELECT E.EmployeeID AS ManagerID,(E.FirstName+' '+E.LastName) AS Manager
FROM Employees E
JOIN Employees M ON M.ManagerID = E.EmployeeID
GROUP BY E.EmployeeID, E.FirstName,E.LastName
HAVING COUNT(E.EmployeeID)=5 

-- 33. Write a SQL query to find all employees along with their managers. 
--     For employees that do not have manager display the value "(no manager)".
SELECT (E.FirstName+' '+E.LastName) AS Employee,
		ISNULL(M.FirstName+' '+M.LastName,'No Manager') AS Manager
FROM Employees E
LEFT JOIN Employees M ON E.ManagerID = M.EmployeeID

-- 34. Write a SQL query to find the names of all employees whose last name is exactly 5 characters long. 
--     Hint: Use the built-in LEN(str) function.
SELECT E.FirstName+' '+E.LastName AS Employee
FROM Employees E
WHERE LEN(E.LastName)=5

-- 35. Write a SQL query to display the current date and time in the following format "day.month.year hour:minutes:seconds:milliseconds".
--     Hint: Search in Google to find how to format dates in SQL Server.
SELECT FORMAT(GETDATE(),'dd.MM.yyyy HH:mm:ss:fff')

-- 36. Write a SQL statement to create a table Users. Users should have username, password, full name and last login time.
--     - Choose appropriate data types for the table fields. Define a primary key column with a primary key constraint.
--     - Define the primary key column as identity to facilitate inserting records.
--     - Define unique constraint to avoid repeating usernames.
--     - Define a check constraint to ensure the password is at least 5 characters long.
CREATE TABLE Users(
	UserId int IDENTITY,
	Username nvarchar(50) NOT NULL,
	Password nvarchar(50) NOT NULL CHECK(LEN(Password)>=3),
	FullName nvarchar(50) NOT NULL,
	LastLoginTime datetime,
	CONSTRAINT PK_Users PRIMARY KEY(UserID),
	CONSTRAINT U_Users UNIQUE(Username),
	)
	GO

-- 37. Write SQL statements to insert in the Users table the names of all employees from the Employees table.
--     - Combine the first and last names as a full name.
--     - For username use the first letter of the first name + the last name (in lowercase).
--     - Use the same for the password, and NULL for last login time.
INSERT INTO Users(Username,Password,FullName)
	SELECT 
		   LOWER(CONCAT(LEFT(E.FirstName,1),E.MiddleName,E.LastName)),
		   LOWER(CONCAT(LEFT(E.FirstName,1),E.LastName)),
		   E.FirstName+' '+E.LastName
	FROM Employees E
GO
-- 38. Write a SQL statement that changes the password to NULL for all users that have not been in the system since 10.03.2010.
UPDATE Users 
SET Password = NULL
WHERE LastLoginTime<=CONVERT(datetime,'10-03-2010')

-- 39. Write a SQL statement that deletes all users without passwords (NULL password).
DELETE FROM Users 
WHERE Password = NULL

-- 40. Write a SQL query to display the average employee salary by department and job title.
SELECT D.Name,E.JobTitle,AVG(E.Salary)
FROM Employees E
JOIN Departments D ON E.DepartmentID = D.DepartmentID
GROUP BY D.Name,E.JobTitle

-- 41. Write a SQL query to display the minimal employee salary by department and job title along with the name of some of the employees that take it.
SELECT D.Name,E.JobTitle,MIN(E.Salary),MAX(CONCAT(E.FirstName,' ',E.LastName)) AS Employee
FROM Employees E
JOIN Departments D ON E.DepartmentID = D.DepartmentID
GROUP BY D.Name,E.JobTitle

-- 42. Write a SQL query to display the town where maximal number of employees work.
SELECT TOP 1 T.Name AS Town,COUNT(E.EmployeeID) AS Employees
FROM Employees E
JOIN Addresses A ON E.AddressID = A.AddressID
JOIN Towns T ON T.TownID = A.TownID
GROUP BY T.Name

-- 43. Write a SQL query to display the number of managers from each town.
SELECT T.Name AS Town,COUNT(E.EmployeeID) AS Managers
FROM Employees E
JOIN Employees M ON M.EmployeeID = E.ManagerID
JOIN Addresses A ON E.AddressID = A.AddressID
JOIN Towns T ON T.TownID = A.TownID
GROUP BY T.Name
