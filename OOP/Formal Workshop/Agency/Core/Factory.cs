﻿using System;
using Agency.Core.Contracts;
using Agency.Models.Contracts;
using Agency.Models.Travel;
using Agency.Models.Vehicles;

namespace Agency.Core
{
    public class Factory : IFactory
    {
        private static IFactory instance;
        public static IFactory Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new Factory();
                }

                return instance;
            }
        }
        
        public IBus CreateBus(int passengerCapacity, double pricePerKilometer, bool hasFreeTv)
        {
            return new Bus(passengerCapacity, pricePerKilometer, hasFreeTv);
        }

        public IAirplane CreateAirplane(int passengerCapacity, double pricePerKilometer, bool isLowCost)
        {
            return new Airplane(passengerCapacity, pricePerKilometer, isLowCost);
        }

        public ITrain CreateTrain(int passengerCapacity, double pricePerKilometer, int carts)
        {
            return new Train(passengerCapacity, pricePerKilometer, carts);
        }
        
        public IJourney CreateJourney(string startLocation, string destination, int distance, IVehicle vehicle)
        {
            return new Journey(startLocation, destination, distance, vehicle);
        }

        public ITicket CreateTicket(IJourney journey, double administrativeCosts)
        {
            return new Ticket(journey, administrativeCosts);
        }
    }
}
