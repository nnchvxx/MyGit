﻿using Agency.Models.Contracts;
using Agency.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agency.Models.Vehicles
{
    class Train : Vehicle, ITrain
    {
        private int carts;
        private int passengerCapacity;
        public Train(int passengerCapacity, double pricePerKilometer, int carts)
            :base(passengerCapacity, pricePerKilometer, VehicleType.Land)
        {
            this.Carts = carts;
        }
        public override int PassangerCapacity 
        {
            get => this.passengerCapacity;
            protected set
            {
                if (value <30 || value >150)
                {
                    throw new ArgumentException("A train cannot have less than 30 passengers or more than 150 passengers.");
                }
                this.passengerCapacity = value;
            }
            
        }
        
        public int Carts
        {
            get => this.carts;
            private set
            {
                if (value<1 || value > 15)
                {
                    throw new ArgumentException("A train cannot have less than 1 cart or more than 15 carts.");
                }
                this.carts = value;
            }
        }
        public override string ToString()
        {
            return $"Train ----\n{base.ToString()}\nCarts amount: {this.carts}";
        }

    }
}
