﻿using Agency.Models.Contracts;
using Agency.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agency.Models.Vehicles
{
    class Airplane : Vehicle, IAirplane
    {
        public Airplane(int passengerCapacity, double pricePerKilometer, bool isLowCost)
            : base(passengerCapacity, pricePerKilometer, VehicleType.Air)
        {
            this.IsLowCost = isLowCost;
        }
        public bool IsLowCost{get;}
        public override string ToString()
        {
            return $"Airplane ----\n{base.ToString()}\nIs low-cost: {this.IsLowCost}";
        }
    }
}
