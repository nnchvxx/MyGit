﻿using Agency.Models.Contracts;
using Agency.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agency.Models.Vehicles
{
    public abstract class Vehicle : IVehicle
    {
        private double pricePerKilometer;
        private int passangerCapacity;
        

        protected Vehicle(int passangerCapacity, double pricePerKilometer, VehicleType type)
        {
            this.PassangerCapacity = passangerCapacity;
            this.PricePerKilometer = pricePerKilometer;
            this.Type = type;
        }
        public double PricePerKilometer 
        {
            get => this.pricePerKilometer;
            private set
            {
                if (value<0.10 || value > 2.50)
                {
                    throw new ArgumentException("A vehicle with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!");
                }
                this.pricePerKilometer = value;
            }
        }
        public virtual int PassangerCapacity
        {
            get => this.passangerCapacity;
            protected set
            {
                if (value < 1 || value > 800)
                {
                    throw new ArgumentException("A vehicle with less than 1 passengers or more than 800 passengers cannot exist!");
                }
                this.passangerCapacity = value;
            }
        }
        public VehicleType Type { get; }

        public override string ToString()
        {
            return $"Passenger capacity: {this.PassangerCapacity}\nPrice per kilometer: {this.pricePerKilometer}\n" +
                $"Vehicle Type: {this.Type}";
        }
        
    }
}
