﻿using Agency.Models.Contracts;
using Agency.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agency.Models.Vehicles
{
    class Bus : Vehicle, IBus
    {
        private int passengerCapacity;
        public Bus(int passengerCapacity, double pricePerKilometer, bool hasFreeTv)
            : base(passengerCapacity, pricePerKilometer, VehicleType.Land)
        {
            this.HasFreeTv = hasFreeTv;
        }
        public bool HasFreeTv { get; }
        public override int PassangerCapacity
        {
            get => this.passengerCapacity;
            protected set
            {
                if (value<10 || value > 50)
                {
                    throw new ArgumentException("A bus cannot have less than 10 passengers or more than 50 passengers.");
                }
                this.passengerCapacity = value;
            }
        }
        public override string ToString()
        {
            return $"Bus ----\n{base.ToString()}\nHas free TV: {this.HasFreeTv}";
        }
    }
}
