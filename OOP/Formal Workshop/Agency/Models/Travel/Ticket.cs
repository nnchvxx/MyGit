﻿using Agency.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agency.Models.Travel
{
    class Ticket : ITicket
    {
        public Ticket(IJourney journey, double administrativeCosts)
        {
            this.Journey = journey;
            this.AdministrativeCosts = administrativeCosts;
        }
        public double AdministrativeCosts { get; }
        public IJourney Journey { get; }

        public double CalculatePrice()
        {
            return this.Journey.CalculatePrice() + this.AdministrativeCosts;
        }
        public override string ToString()
        {
            return $"Ticket ----\nDestination: {this.Journey.Destination}\nPrice: {this.CalculatePrice()}";
        }
    }
}
