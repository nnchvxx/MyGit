﻿using Agency.Models.Contracts;
using Agency.Models.Vehicles;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agency.Models.Travel
{
    class Journey : IJourney
    {
        private string startLocation;
        private string destination;
        private int distance;
        public Journey(string startLocation, string destination, int distance, IVehicle vehicle)
        {
            this.StartLocation = startLocation;
            this.Destination = destination;
            this.Distance = distance;
            this.Vehicle = vehicle;
        }
        public string StartLocation
        {
            get => this.startLocation;
            private set
            {
                if (value.Length < 5 || value.Length > 25)
                {
                    throw new ArgumentException("The length of StartLocation must be between 5 and 25 symbols.");
                }
                this.startLocation = value;
            }
        }
        public string Destination
        {
            get => this.destination;
            private set
            {
                if (value.Length < 5 || value.Length > 25)
                {
                    throw new ArgumentException("The length of Destination must be between 5 and 25 symbols.");
                }
                this.destination = value;
            }
        }
        public int Distance
        {
            get => this.distance;
            private set
            {
                if (value<5 || value >5000)
                {
                    throw new ArgumentException("Distance must be between 5 and 5000 kilometers.");
                }
                this.distance = value;
            }
        }
        public IVehicle Vehicle { get; }

        public double CalculatePrice()
        {
            return this.Vehicle.PricePerKilometer * this.distance;
        }
        public override string ToString()
        {
            return $"Journey ----\nStart location: {this.startLocation}\nDestination: {this.destination}\nDistance: {this.distance}\n" +
                $"Vehicle type: {this.Vehicle.Type}\nTravel costs: {this.CalculatePrice()}";
        }

    }
}
