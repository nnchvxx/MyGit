﻿using Agency.Models.Enums;

namespace Agency.Models.Contracts
{
    public interface IVehicle
    {
        public int PassangerCapacity { get; }
        public double PricePerKilometer { get; }
        public VehicleType Type
        {
            get;
        }
        

    }
}