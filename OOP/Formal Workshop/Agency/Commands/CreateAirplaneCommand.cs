﻿using Agency.Commands.Abstracts;
using System;
using System.Collections.Generic;

namespace Agency.Commands
{
    public class CreateAirplaneCommand : Command
    {
        public CreateAirplaneCommand(IList<string> commandParameters)
            : base(commandParameters)
        {
        }

        public override string Execute()
        {
            int passengerCapacity;
            double pricePerKilometer;
            bool isLowCost;

            try
            {
                passengerCapacity = int.Parse(this.CommandParameters[0]);
                pricePerKilometer = double.Parse(this.CommandParameters[1]);
                isLowCost = bool.Parse(this.CommandParameters[2]);
            }
            catch
            {
                throw new ArgumentException("Failed to parse CreatAirplane command parameters.");
            }

            var airPlane = this.Factory.CreateAirplane(passengerCapacity, pricePerKilometer, isLowCost);
            this.Database.Vehicles.Add(airPlane);

            return $"Vehicle with ID {this.Database.Vehicles.Count - 1} was created.";
        }
    }
}
