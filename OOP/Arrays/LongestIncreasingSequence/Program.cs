﻿using System;

namespace LongestIncreasingSequence
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[n];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = int.Parse(Console.ReadLine());
            }
            Console.WriteLine(findSequence(arr));
 
        }
        static int findSequence(int[] arr)
        {
            int counter = 1;
            int biggest = 0;
            for (int i = 0; i < arr.Length-1; i++)
            {
                if (arr[i]<arr[i+1])
                {
                    counter++;
                }
                else
                {
                    if (counter>biggest)
                    {
                        biggest = counter;
                    }
                    counter = 1;
                    
                }
            }
            return biggest;
        }
    }
}
