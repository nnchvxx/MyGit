﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            int n = int.Parse(Console.ReadLine());
            PrintNumbers(n);

        }
        //check if prime number - return bool
        static bool checkIfPrime(int n)
        {
            for (int i = 2; i < n; i++)
            {
                if (n%i==0)
                {
                    return false;
                }
            }
            return true;
        }
        
        static void PrintNumbers(int n)
        {
            
            for (int i = 1; i <= n; i++)
            {
                if (checkIfPrime(i))
                {
                    for (int j = 1; j <= i ; j++)
                    {
                        if (checkIfPrime(j))
                        {
                            Console.Write(1);
                        }
                        else
                        {
                            Console.Write(0);
                        }
                    }
                    Console.WriteLine();
                }
            }
            
        }


    }
}
