﻿using System;
using System.Collections.Generic;

namespace bigNumbers
{
    class Program
    {
        static void Main(string[] args)
        {

            string[] arrStr = Console.ReadLine().Split(' ');
            int sizeArray1 = int.Parse(arrStr[0]);
            int sizeArray2 = int.Parse(arrStr[1]);

            string[] firstArr = Console.ReadLine().Split(' ');
            string[] secondArr = Console.ReadLine().Split(' ');

            List<int> list1 = new List<int>();
            List<int> list2 = new List<int>();


            for (int i = 0; i < sizeArray1; i++)
            {
                
                list1.Add(int.Parse(firstArr[i]));
            }
            for (int i = 0; i < sizeArray2; i++)
            {
                
                list2.Add(int.Parse(secondArr[i]));
            }
            
            List<int> sum = new List<int>();

            if (list1.Count>list2.Count)
            {
                for (int i = list2.Count; i < list1.Count; i++)
                {
                    list2.Add(0);
                }
            }
            else
            {
                for (int i = list1.Count; i < list2.Count; i++)
                {
                    list1.Add(0);
                }
            }

            for (int i = 0; i < list2.Count; i++)
            {
                if ((list1[i] + list2[i]) >= 10)
                {
                    list1[i + 1] += (list1[i] + list2[i]) / 10;
                    sum.Add((list1[i] + list2[i]) % 10);
                }
                else
                {
                    sum.Add((list1[i] + list2[i]));
                }
            }
            for (int i = 0; i < sum.Count; i++)
            {
                Console.Write("{0} ",sum[i]);
            }



        }


    }
}