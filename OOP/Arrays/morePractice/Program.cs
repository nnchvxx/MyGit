﻿using System;

namespace PracticeInClass
{
    class Program
    {
        static (bool success, string message) ProcessCommand(string command, string[,] userTable)
        {
            string[] commandArg = command.Split(' ');
            if (commandArg.Length<3)
            {
                return (false, "Too few parameters");
            }
            string username = commandArg[1];
            if (username.Length<3)
            {
                return (false, "Username must be at least 3 characters long.");
            }
            string password = commandArg[2];
            if (password.Length<3)
            {
                return (false, "Password must be at least 3 characters long.");
            }
            return commandArg[0] switch
            {
                "register" => Register(username, password, userTable),
                "delete" => Delete(username, password, userTable),
                _ => (false, "Invalid command")
            };


        }

         static (bool success, string message) Delete(string username, string password, string[,] userTable)
        {
            int accountIndex = FindIndex(username, userTable);
            if (accountIndex == -1 || userTable[accountIndex,1] != password)
            {
                return (false, "Invalid account/password.");
            }
            userTable[accountIndex, 0] = null;
            userTable[accountIndex, 1] = null;
            return (true, "Deleted account.");
        }

        static int FindIndex(string username, string[,] userTable)
        {
            for (int i = 0; i < userTable.GetLength(0); i++)
            {
                if ((userTable[i,0]) == username)
                {
                    return i;
                }
            }
            return -1;
        }
        static void LogColor(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        static (bool success, string message) Register(string username, string password, string[,] userTable)
        {
            if (FindIndex(username, userTable) >= 0)
            {
                return (false, "User already exist");
            }
            int freeIndex = FindIndex(null, userTable);
            if (freeIndex == -1)
            {
                return (false, "The system supports a maximum number of 4 user.");
            }
            userTable[freeIndex, 0] = username;
            userTable[freeIndex, 1] = password;

            return (true, "Registered user.");
        }

        static void Main(string[] args)
        {

            string command = Console.ReadLine();
            string[,] userTable = new string[4, 2];

            while (command != "end")
            {
                var (success, message) = ProcessCommand(command, userTable);
                LogColor(message, success ? ConsoleColor.DarkGreen : ConsoleColor.Red);
                command = Console.ReadLine();
            }

        }
    }
}
