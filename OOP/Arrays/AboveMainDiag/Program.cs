﻿using System;

namespace AboveMainDiag
{
    class Program
    {
        static void Main(string[] args)
        {

            int n = int.Parse(Console.ReadLine());
            ulong[,] matrix = new ulong[n,n];
            ulong sum = 0;
            
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    matrix[i, j] = (ulong)Math.Pow(2,i+j);

                }
                
            }
            for (int i = 0; i < n-1; i++)
            {
                for (int j = i+1; j < n; j++)
                {
                    sum += matrix[i, j];
                }
            }
            Console.WriteLine(sum);

        }
    }
}
