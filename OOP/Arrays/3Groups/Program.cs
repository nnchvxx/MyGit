﻿using System;
using System.Collections.Generic;

namespace _3Groups
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] arr = Console.ReadLine().Split(' ');
            
            List<string> list1 = new List<string>();
            List<string> list2 = new List<string>();
            List<string> list3 = new List<string>();

            for (int i = 0; i < arr.Length; i++)
            {
                int compare = int.Parse(arr[i]);
                if (compare%3==0)
                {
                    list1.Add(arr[i]);
                }
                else if (compare%3==1)
                {
                    list2.Add(arr[i]);
                }
                else if(compare%3==2)
                {
                    list3.Add(arr[i]);
                }
            }
            Console.WriteLine(string.Join(" ",list1));
            Console.WriteLine(string.Join(" ",list2));
            Console.WriteLine(string.Join(" ",list3));

        }
    }
}
