﻿using System;

namespace SpiralMatrix
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[][] matrix = new int[n][];
            for (int i = 0; i < n; i++)
            {
                matrix[i] = new int[n];
            }
            int colStart = 0;
            int colEnd = n - 1;
            int rowStart = 0;
            int rowEnd = n - 1;
            int number = 1;

            while (colStart<=colEnd && rowStart<=rowEnd)
            {
                for (int i = colStart; i <= colEnd; i++)
                {
                    matrix[rowStart][i] = number++;
                }
                for (int i = rowStart+1; i <= rowEnd ; i++)
                {
                    matrix[i][colEnd] = number++;
                }
                for (int i = colEnd-1; i >=colStart ; i--)
                {
                    matrix[rowEnd][i] = number++;
                }
                for (int i = rowEnd-1; i >rowStart ; i--)
                {
                    matrix[i][colStart] = number++;
                }
                colStart++;
                rowStart++;
                colEnd--;
                rowEnd--;
            }
            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix[i].Length; j++)
                {
                    Console.Write(matrix[i][j]+" ");
                }
                Console.WriteLine();
            }


        }
    }
}
