﻿using System;

namespace SymmetricArrays
{
    class Program
    {
        static void Main(string[] args)
        {

            int n = int.Parse(Console.ReadLine());

            string[][] arr = new string[n][];
            bool isSym = false;
            
            
            for (int i = 0; i < arr.Length; i++)
            {
                string[] arrStr = Console.ReadLine().Split(' ');
                arr[i] = arrStr;
            }

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i].Length<2)
                {
                    isSym = true;
                }
                else 
                {
                    for (int j = 0; j < arr[i].Length / 2; j++)
                    {

                        int a = arr[i].Length - j - 1;
                        if (int.Parse(arr[i][j]) != int.Parse(arr[i][a]))
                        {
                            Console.WriteLine("No");
                            isSym = false;
                            break;
                        }
                        else
                        {
                            isSym = true;
                        }
                    }
                    
                }
                if (isSym == true)
                {
                    Console.WriteLine("Yes");
                }
                
            }

            

        }
    }
}
