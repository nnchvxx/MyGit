﻿ using System;

namespace justPractice
{
    class Program
    {
        static bool ValidateUsername(string username)
        {
            bool valid = true;
            if (username.Length < 3)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Username must be at least 3 characters long.");
                valid = false;
            }
            return valid;
        }
        static bool ValidateArgs(int length)
        {
            bool valid = true;
            if (length < 3)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Too few parameters.");
                valid = false;
            }
            return valid;
        }
        static bool ValidatePassword(string password)
        {
            bool valid = true;
            if (password.Length < 3)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Password must be at least 3 characters long.");
                valid = false;
            }
            return valid;
        }
        static bool CheckUsernameExist(string username, string[,] usertable)
        {
            bool valid = true;
            for (int i = 0; i < usertable.GetLength(0); i++)
            {
                if (usertable[i,0] != username)
                {
                    valid = true;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Username already exists.");
                    valid = false;
                }
            }
            return valid;
        }
        static bool DeleteAccount(string[,] usertable, string username, string password)
        {
            bool valid = true;
            int index = -1;
            for (int i = 0; i < usertable.GetLength(0); i++)
            {
                if (usertable[i, 0] == username &&
                    usertable[i, 1] == password)
                {
                    index = i;
                    usertable[index, 0] = null;
                    usertable[index, 1] = null;

                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.WriteLine("Deleted account.");
                    valid = false;
                    break;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Invalid account/password.");
                    valid = true;
                    break;
                }
            }
            return valid;
        }
        static bool CheckFreeSlot(string[,] usertable, string username, string password)
        {
            bool valid = true;
            
            for (int i = 0; i < usertable.GetLength(0); i++)
            {
                if (usertable[i, 0] == null)
                {
                    
                    usertable[i, 0] = username;
                    usertable[i, 1] = password;
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.WriteLine("Registered user.");
                    valid = true;
                    break;
                }
                else if(i==3)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("The system supports a maximum number of 4 users.");
                    break;
                    
                }
            }
            return valid;
        }
        static void Main(string[] args)
        {
            string command = Console.ReadLine();
            string[,] userTable = new string[4, 2];

            // main loop
            while (command != "end")
            {
                string[] commandArgs = command.Split(" ");
                switch (commandArgs[0])
                {
                    case "register":
                        {
                            // validate arguments
                            if (ValidateArgs(commandArgs.Length) == false)
                            {
                                break;
                            }

                            string username = commandArgs[1];
                            string password = commandArgs[2];

                            // validate username
                            if (ValidateUsername(username) == false)
                            {
                                break;
                            }

                            // validate password
                            if (ValidatePassword(password) == false)
                            {
                                break;
                            }

                            // check if username exists
                            if (CheckUsernameExist(username, userTable) == false)
                            {
                                break;
                            }

                            // find free slot

                            if (CheckFreeSlot(userTable, username, password) == false)
                            {
                                break;
                            }
                            break;
                        }
                    case "delete":
                        {
                            // validate arguments
                            if (ValidateArgs(commandArgs.Length) == false)
                            {
                                break;
                            }

                            string username = commandArgs[1];
                            string password = commandArgs[2];

                            // validate username
                            if (ValidateUsername(username) == false)
                            {
                                break;
                            }

                            // validate password
                            if (ValidatePassword(password) == false)
                            {
                                break;
                            }

                            // find account to delete
                            if (DeleteAccount(userTable,username,password)==true)
                            {
                                break;
                            }
                            break;
                        }
                }

                Console.ResetColor();
                // read next command
                command = Console.ReadLine();
            }
        }
    }
}
