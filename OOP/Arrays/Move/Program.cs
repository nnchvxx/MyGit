﻿using System;

namespace Move
{
    class Program
    {
        static void Main(string[] args)
        {
            int startingPoint = int.Parse(Console.ReadLine());
            string[] str = Console.ReadLine().Split(',');
            int[] arr = new int[str.Length];
            for (int i = 0; i < str.Length; i++)
            {
                arr[i] = int.Parse(str[i]);
            }

            string input = Console.ReadLine();

            int sumForward = 0;
            int sumBackwards = 0;

            while (input != "exit")
            {
                string[] move = input.Split(' ');
                int steps = int.Parse(move[0]);
                string direction = move[1];
                int size = int.Parse(move[2]);

                if (direction == "forward")
                {
                    for(int i = 0; i < steps; i++)
                    {
                        startingPoint += size % arr.Length;
                        if (startingPoint >= arr.Length)
                        {
                            startingPoint -= arr.Length;
                        }
                        sumForward += arr[startingPoint];
                    }
                }
                if (direction == "backwards")
                {
                    for (int i = 0; i < steps; i++)
                    {
                        startingPoint -= size % arr.Length;
                        if (startingPoint < 0)
                        {
                            startingPoint += arr.Length;
                        }
                        sumBackwards += arr[startingPoint];
                    }
                }
                input = Console.ReadLine();

            }
            Console.WriteLine("Forward: " + sumForward);
            Console.WriteLine("Backwards: " + sumBackwards);
        }
        
        
    }
}
