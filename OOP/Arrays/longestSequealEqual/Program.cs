﻿using System;

namespace longestSequealEqual
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[n];

            int sequal = 1;
            int biggestS = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = int.Parse(Console.ReadLine());
            }
            for (int i = 0; i < arr.Length-1; i++)
            {
                
                if (arr[i]==arr[i+1])
                {
                    sequal++;
                    if (sequal > biggestS)
                    {
                        biggestS = sequal;
                    }
                }
                else
                {
                    sequal = 1;
                }

            }
            Console.WriteLine(biggestS);
        }
    }
}
