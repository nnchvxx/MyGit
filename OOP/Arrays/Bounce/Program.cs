﻿using System;

namespace Bounce
{
    class Program
    {
        static void Main(string[] args)
        {

            
            string[] arr = Console.ReadLine().Split(' ');
            int rows = int.Parse(arr[0]);
            int cols = int.Parse(arr[1]);

            long[,] matrix = new long[rows,cols];
            long sum = 0;

            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < cols; col++)
                {
                    matrix[row, col] = (long)Math.Pow(2, row + col);
                }
            }
            int currentCol = 0;
            int currentRow = 0;
            string direction = "down-right";
            bool isInBound = true;

            while (isInBound)
            {
                if (rows==1 || cols==1)
                {
                    sum = 1;
                    break;
                }
                while (direction=="down-right")
                {
                    sum += matrix[currentRow,currentCol];
                    currentCol++;
                    currentRow++;
                    if (currentCol==cols && currentRow==rows)
                    {
                        isInBound = false;
                        break;
                    }
                    if (currentRow==rows && currentCol<cols)
                    {
                        currentRow -= 2;
                        direction = "up-right";
                    }
                    else if (currentRow<=rows && currentCol==cols)
                    {
                        currentCol -= 2;
                        direction = "down-left";
                    }
                }
                while (direction=="up-right")
                {
                    sum += matrix[currentRow, currentCol];
                    currentCol++;
                    currentRow--;
                    if (currentRow==-1 && currentCol==cols)
                    {
                        isInBound = false;
                        break;
                    }
                    if (currentRow<0 && currentCol<cols)
                    {
                        currentRow += 2;
                        direction = "down-right";
                    }
                    else if (currentRow>=0 && currentCol==cols)
                    {
                        currentCol -= 2;
                        direction = "up-left";
                    }
                }
                while (direction=="up-left")
                {
                    sum += matrix[currentRow, currentCol];
                    currentCol--;
                    currentRow--;
                    if (currentRow == -1 && currentCol == -1)
                    {
                        isInBound = false;
                        break;
                    }
                    
                    if (currentRow<0 && currentCol>0)
                    {
                        currentRow += 2;
                        direction = "down-left";
                    }
                    else if (currentRow>=0 && currentCol<0)
                    {
                        currentCol += 2;
                        direction = "up-right";
                    }
                }
                while (direction=="down-left")
                {
                    sum += matrix[currentRow, currentCol];
                    currentRow++;
                    currentCol--;
                    if (currentRow==rows && currentCol ==-1)
                    {
                        isInBound = false;
                        break;
                    }
                    if (currentRow==rows && currentCol>0)
                    {
                        currentRow -= 2;
                        direction = "up-left";
                    }
                    else if (currentRow<=rows && currentCol<0)
                    {
                        currentCol += 2;
                        direction = "down-right";
                    }

                }
            }
            Console.WriteLine(sum);
            
        }
    }
}
