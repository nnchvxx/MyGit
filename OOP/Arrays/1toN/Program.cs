﻿using System;

namespace _1toN
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            PrintNumbers(n);

        }
        static bool checkIfPrime(int n)
        {
            for (int i = 2; i < n; i++)
            {
                if (n % i == 0)
                {
                    return false;
                }
            }
            return true;
        }
        static void PrintNumbers(int n)
        {

            for (int i = 1; i <= n; i++)
            {
                if (checkIfPrime(i))
                {
                    Console.Write(i + " ");
                }
            }
        }
    }
}
