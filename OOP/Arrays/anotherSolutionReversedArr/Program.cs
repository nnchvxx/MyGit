﻿using System;

namespace anotherSolutionReversedArr
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] arr = Console.ReadLine().Split(' ');
            Array.Reverse(arr);

            Console.WriteLine(string.Join(", ", arr));
            

        }
    }
}
