﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace PrimeNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = Console.ReadLine();
            string[] arr = Regex.Split(str, @"\D+");
            int biggest = -1;

            

            foreach (string item in arr)
            {
                if (!string.IsNullOrEmpty(item))
                {
                    int a = int.Parse(item);
                    if (a%2==0 && a>biggest)
                    {
                        biggest = a;
                    }
                    
                }
            }
            Console.WriteLine(biggest);
            


        }
        
    }
}
