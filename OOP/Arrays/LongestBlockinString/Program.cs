﻿using System;
using System.Text;

namespace LongestBlockinString
{
    class Program
    {
        static void Main(string[] args)
        {
            string sb = Console.ReadLine();

            CheckLongestBlock(sb);
        }

        static void CheckLongestBlock(string str)
        {
            int counter = 1;
            int biggest = 0;
            char ch = '\0';
            if (str.Length == 1)
            {
                Console.WriteLine(str[0]);
                return;
            }
            for (int i = 0; i < str.Length - 1; i++)
            {
                if (str[i] == str[i + 1])
                {
                    counter++;

                }
                else
                {
                    counter = 1;
                }
                if (counter > biggest)
                {
                    biggest = counter;
                    ch = str[i];
                }

            }
            for (int i = 0; i < biggest; i++)
            {
                Console.Write(ch);
            }
            
        }
    }
       
        
    
}
