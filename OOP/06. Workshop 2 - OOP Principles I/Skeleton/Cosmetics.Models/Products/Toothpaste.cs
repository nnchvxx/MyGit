﻿using Cosmetics.Common;
using Cosmetics.Contracts;
using System;
using System.Collections.Generic;

namespace Cosmetics.Products
{
    public class Toothpaste : Product, IToothpaste
    {
        private string  ingredients;
        public Toothpaste(string name, string brand, decimal price, GenderType gender, string ingredients)
           : base(name, brand, price, gender)
        {
            this.Ingredients = ingredients ?? throw new ArgumentNullException();           
        }

        public string  Ingredients
        {
            get
            {
                return this.ingredients;
            }
            set
            {
                this.ingredients = value;
            }
        }

        public override string Print()
        {
            string info = $"#{this.Name} {this.Brand}\n #Price: ${this.Price}\n #Gender: " +
                $"{this.Gender}\n #Ingredients: {this.Ingredients}\n ===";
            return info;
        }
    }
}