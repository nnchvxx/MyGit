﻿using System;
using System.Collections.Generic;
using System.Text;
using Cosmetics.Common;
using Cosmetics.Contracts;

namespace Cosmetics.Products
{
    public abstract class Product : IProduct
    {
        private string name;
        private string brand;
        private decimal price;
        private GenderType gender;

        public Product(string name, string brand, decimal price, GenderType gender)
        {
            this.Name = name ?? throw new ArgumentNullException("Name cannot be null");
            this.Brand = brand ?? throw new ArgumentNullException("Brand cannot be null");
            this.Price = price;
            this.Gender = gender;
           
        }

        public virtual string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (value.Length < 3 || value.Length > 10)
                {
                    throw new ArgumentOutOfRangeException("Minimum name’s length is 3 symbols and maximum is 10 symbols.");
                }
                this.name = value;
            }
        }


        public virtual string Brand
        {
            get
            {
                return this.brand;
            }
            set
            {
                if (value.Length < 2 || value.Length > 10)
                {
                    throw new ArgumentOutOfRangeException("Minimum brand name’s length is 2 symbols and maximum is 10 symbols.");
                }
                this.brand = value;
            }
        }

        public virtual decimal Price
        {
            get
            {
                return this.price;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("Price cannot be negative.");
                }
                this.price = value;
            }
        }

        public GenderType Gender
        {
            get
            {
                return this.gender;
            }
            set
            {
                this.gender = value;
            }
        }

        public abstract string Print();
    }
}
