﻿using System;
using System.Collections.Generic;
using System.Text;
using Cosmetics.Common;
using Cosmetics.Contracts;

namespace Cosmetics.Products
{
    public class Cream : Product, ICream
    {
        private Scent scent;
        private string name;
        private string brand;
        private decimal price;

        public Cream(string name, string brand, decimal price, GenderType gender, Scent scent)
           : base(name, brand, price, gender)
        {
            this.Scent = scent;
        }

        public Scent Scent
        {
            get
            {
                return this.scent;
            }
            set
            {
                this.scent = value;
            }
        }

        public override string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (value.Length < 3 || value.Length > 15)
                {
                    throw new ArgumentOutOfRangeException("Minimum cream name’s length is 3 symbols and maximum is 15 symbols.");
                }
                this.name = value;
            }
        }


        public override string Brand
        {
            get
            {
                return this.brand;
            }
            set
            {
                if (value.Length < 3 || value.Length > 15)
                {
                    throw new ArgumentOutOfRangeException("Minimum cream brand name’s length is 3 symbols and maximum is 15 symbols.");
                }
                this.brand = value;
            }
        }

        public override decimal Price
        {
            get
            {
                return this.price;
            }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException("Price cannot be negative or zero.");
                }
                this.price = value;
            }
        }

        public override string Print()
        {
            string info = $"#{this.Name} {this.Brand}\n #Price: ${this.Price}\n #Gender: " +
                $"{this.Gender}\n #Scent {this.Scent}\n ===";
            return info;
        }
    }
}
