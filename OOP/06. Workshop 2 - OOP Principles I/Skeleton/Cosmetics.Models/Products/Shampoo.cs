﻿using Cosmetics.Common;
using Cosmetics.Contracts;
using System;

namespace Cosmetics.Products
{
    public class Shampoo : Product, IShampoo
    {
        private uint milliliters;
        private UsageType usageType;

        public Shampoo(string name, string brand, decimal price, GenderType gender, uint milliliters, UsageType usageType)
            :base(name, brand, price, gender)
        {
          
            this.Milliliters = milliliters;
            this.UsageType = usageType;
        
        }   

        public uint Milliliters
        {
            get
            {
                return this.milliliters;
            }
            set
            {
                if(value < 0)
                {
                    throw new ArgumentOutOfRangeException("Milliliters are not negative number.");
                }
                this.milliliters = value;
            }
        }

        public UsageType UsageType
        {
            get
            {
                return this.usageType;
            }
            set
            {
                this.usageType = value;
            }
        }

        public override string Print()
        {
            string info = $"#{this.Name} {this.Brand}\n #Price: ${this.Price}\n #Gender: " +
                $"{this.Gender}\n #Milliliters: {this.Milliliters}\n #Usage: {this.UsageType}\n ===";
            return info;
        }
    }
}
