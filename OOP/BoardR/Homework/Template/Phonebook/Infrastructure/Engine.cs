﻿using PhonebookApp.Models;
using System;

namespace PhonebookApp.Infrastructure
{
    public static class Engine
    {
        public static void Run()
        {
           Phonebook.SeedContacts();

            while (true)
            {
                // input
                var input = Console.ReadLine();

                // process
                try
                {
                    var result = CommandProcessor.ProcessCommand(input);
                    Console.WriteLine(result);
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
