﻿using PhonebookApp.Commands;
using PhonebookApp.Models;
using System;

namespace PhonebookApp.Infrastructure
{
    public static class CommandProcessor
    {
        public static string ProcessCommand(string input)
        {
            var args = input.Split();

            var command = new AddContactCommand();
            return command.Execute(args);
        }
    }
}
