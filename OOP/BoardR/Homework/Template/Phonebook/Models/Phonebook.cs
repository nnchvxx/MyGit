﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PhonebookApp.Models
{
    public static class Phonebook
    {
        private static readonly List<Contact> contacts = new List<Contact>();

        public static void SeedContacts()
        {
            if (!contacts.Any())
            {
                string json = File.ReadAllText(@"../../../contacts.json");
                var seededContacts = JsonConvert.DeserializeObject<List<Contact>>(json);

                foreach (var contact in seededContacts)
                {
                    contacts.Add(contact);
                }
            }
        }

        public static void AddContact(Contact contact)
        {
            CheckContactIsUnique(contact);
            contacts.Add(contact);
        }
        public static string RemoveContact(string name)
        {
            foreach (var contact in contacts)
            {
                if (contact.Name==name)
                {
                    contacts.Remove(contact);
                    return "Contact removed.";
                }
            }
            return $"Contact {name} does not exist.";
        }
        public static void UpdateContact(string name, string number)
        {
            foreach (var contact in contacts)
            {
                if (contact.Name == name)
                {
                    contact.PhoneNumber = number;
                    break;
                }
            }
        }
        public static List<Contact> SearchForContacts(string subString)
        {
            List<Contact> searched = new List<Contact>();
            foreach (var contact in contacts)
            {
                if (contact.Name.StartsWith(subString))
                {
                    searched.Add(contact);
                }
            }
            return searched;
        }


        public static string ListContacts(string inputCommand)
        {
            var builder = new StringBuilder();
            List<Contact> sorted = new List<Contact>();
            if (inputCommand == "name")
            {
                sorted = contacts.OrderBy(x => x.Name).ToList();
            }
            else if(inputCommand == "time")
            {
                sorted = contacts.OrderBy(x => x.CreatedOn).ToList();
            }
            else
            {
                sorted = contacts;
            }
            foreach (var contact in sorted)
            {
                builder.AppendLine(contact.GetContactInfo());
            }
            return builder.ToString();
        }
        private static void CheckContactIsUnique(Contact contact)
        {
            if (ContactIsContained(contact)!=null)
            {
                throw new ArgumentException("Contact already exists.");
            }
        }

        private static Contact ContactIsContained(Contact newContact)
        {
            foreach (var contact in contacts)
            {
                if (newContact.Name == contact.Name)
                {
                    return contact;
                }
            }
            return null;
        }
    }
}
