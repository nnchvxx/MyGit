﻿using PhonebookApp.Infrastructure;

namespace PhonebookApp
{
    class Program
    {
        static void Main()
        {
            Engine.Run();
        }
    }
}
