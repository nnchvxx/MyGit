﻿using System;
using System.Collections.Generic;
using System.Text;
using PhonebookApp.Models;

namespace PhonebookApp.Commands
{
    
    class AddContactCommand
    {
        public string Execute(string[] args)
        {
            if (args[0] == "addcontact")
            {
                if (args.Length != 3)
                {
                    throw new ArgumentException("Please provide at least 3 arguments. The first argument should be name, the second - phonenumber");
                }
                var contact = new Contact(args[1], args[2]);
                Phonebook.AddContact(contact);
                return $"Created contact " + contact.GetContactInfo();
            }
            else if (args[0] == "listcontacts")
            {
                if (args.Length > 1)
                {
                    if (args[1] == "name")
                    {
                        return Phonebook.ListContacts(args[1]);
                    }
                    else if (args[1] == "time")
                    {
                        return Phonebook.ListContacts(args[1]);
                    }
                }
            }
            else if (args[0] == "removecontact")
            {
                Console.WriteLine(Phonebook.RemoveContact(args[1]));
            }
            else
            {
                return "Invalid command";
            }
            return Phonebook.ListContacts(args[0]);
        }

    }
}
