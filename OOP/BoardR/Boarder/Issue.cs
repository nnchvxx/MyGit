﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Boarder
{
    class Issue : BoardItem
    {
        private string description;
        public Issue(string title, string description, DateTime dueDate) : base(title, dueDate, Status.Open)
        {
            this.Description = description;
            this.AddEventLog($"Created Issue: {this.ViewInfo()}. Description: {this.Description}");
        }
        public string Description
        {
            get
            {
                return this.description;
            }
            private set
            {
                if (string.IsNullOrEmpty(value))
                {
                    value = "No description";
                }
                this.description = value;
            }
        }
    }
}
