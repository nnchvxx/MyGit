﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Boarder
{
    class Task : BoardItem
    {
        private string assignee;
        private const Status initialStatus = Status.Todo;
        public Task(string title, string assignee, DateTime dueDate) : base(title,dueDate, initialStatus)
        {
            this.EnsureAsigneeIsValid(assignee);
            this.assignee = assignee;
            this.AddEventLog($"Created task: {this.ViewInfo()}");
        }
        
        public string Assignee
        {
            get
            {
                return this.assignee;
            }
            set
            {
                this.EnsureAsigneeIsValid(value);
                this.AddEventLog($"Assignee changed from {this.assignee} to {value}.");
                this.assignee = value;
            }
        }
        private void EnsureAsigneeIsValid(string assignee)
        {
            if (string.IsNullOrWhiteSpace(assignee))
            {
                throw new ArgumentException("Assignee cannot be empty.");
            }
            if (assignee.Length < 5 || assignee.Length > 30)
            {
                throw new ArgumentOutOfRangeException("Please enter assignee between 5 and 30 characters.");
            }
        }
        


    }
}
