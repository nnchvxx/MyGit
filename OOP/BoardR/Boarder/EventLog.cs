﻿using System;

namespace Boarder
{
    public class EventLog
    {
        public EventLog(string description)
        {
            if (string.IsNullOrEmpty(description))
            {
                throw new ArgumentNullException("Description cannot be null or empty!");
            }

            this.Description = description;
            this.Time = DateTime.Now;
        }

        public string Description{ get;}

        public DateTime Time
        {
            get;

            private set; 
        }
        public string ViewInfo()
        {
            string timeAsString = $"{this.Time:yyyyMMdd|HH:mm:ss.ffff}";
            string eventInfo = $"[{timeAsString}] {this.Description}";
            return eventInfo;
        }
    }
}
