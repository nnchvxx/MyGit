﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Boarder.Loggers
{
    interface ILogger
    {
        void Log(string value);
    }
}
