﻿using System;
using System.Collections.Generic;
using OlympicGames.Core.Commands.Abstracts;
using OlympicGames.Core.Contracts;
using OlympicGames.Olympics.Contracts;

namespace OlympicGames.Core.Commands
{
    public class CreateSprinterCommand : Command
    {
        public CreateSprinterCommand(IList<string> commandLine, IOlympicCommittee committee)
            : base(commandLine, committee)
        {
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count<3)
            {
                throw new Exception("Parameters count is not valid!");
            }
            IDictionary<string, double> personalRecords = new Dictionary<string, double>();
            string firstName = this.CommandParameters[0];
            string lastName = this.CommandParameters[1];
            string country = this.CommandParameters[2];
            for (int i = 3; i < this.CommandParameters.Count; i++)
            {
                string[] str = this.CommandParameters[i].Split('/');
                double n = double.Parse(str[1]);
                personalRecords.Add(str[0], n);
            }

            IOlympian sprinter = this.Factory.CreateSprinter(firstName, lastName, country, personalRecords);
            this.Committee.Add(sprinter);
            return $"Created Sprinter\n{sprinter.PrintAthleteInfo()}";
        }
    }
}
