﻿namespace OlympicGames.Olympics.Enums
{
    public enum BoxingCategory
    {
        flyweight,
        featherweight,
        lightweight,
        middleweight,
        heavyweight
    }
}
