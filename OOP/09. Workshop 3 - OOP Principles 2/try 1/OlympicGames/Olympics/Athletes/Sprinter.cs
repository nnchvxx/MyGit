﻿using OlympicGames.Olympics.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace OlympicGames.Olympics.Athletes
{
    class Sprinter : Olympian, ISprinter
    {
        public Sprinter(string firstName, string lastName, string country, IDictionary<string, double> personalRecords)
            : base(firstName, lastName, country)
        {
            this.PersonalRecords = personalRecords;
        }
        public IDictionary<string,double> PersonalRecords { get; private set; }

        public override string PrintAthleteInfo()
        {
            
            if (PersonalRecords.Count == 0 )
            {
                return $"SPRINTER: {base.PrintAthleteInfo()}\nNO PERSONAL RECORDS SET";
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                foreach (var item in PersonalRecords)
                {
                    sb.Append($"\n{item.Key}m: {item.Value}s");
                }
                return $"SPRINTER: {base.PrintAthleteInfo()}\nPERSONAL RECORDS:{sb.ToString()}";
            }
        }
    }
}
