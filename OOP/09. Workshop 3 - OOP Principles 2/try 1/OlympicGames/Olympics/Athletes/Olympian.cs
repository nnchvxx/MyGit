﻿using OlympicGames.Olympics.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace OlympicGames.Olympics.Athletes
{
    abstract class Olympian : IOlympian
    {
        private string firstName;
        private string lastName;
        private string country;
        protected Olympian(string firstName, string lastName, string country)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Country = country;
        }
        public string FirstName
        {
            get
            {
                return this.firstName;
            }
            private set
            {
                if (value.Length < 2 || value.Length > 20)
                {
                    throw new Exception("First name must be between 2 and 20 characters long!");
                }
                this.firstName = value;
            }
        }
        public string LastName
        {
            get
            {
                return this.lastName;
            }
            private set
            {
                if (value.Length < 2 || value.Length > 20)
                {
                    throw new Exception("Last name must be between 2 and 20 characters long!");
                }
                this.lastName = value;
            }
        }
        public string Country
        {
            get
            {
                return this.country;
            }
            private set
            {
                if (value.Length < 3 || value.Length > 25)
                {
                    throw new Exception("Country must be between 3 and 25 characters long!");
                }
                this.country = value;
            }
        }

        public virtual string PrintAthleteInfo()
        {
            return $"{this.firstName} {this.lastName} from {this.country}";
        }
        
    }
}
