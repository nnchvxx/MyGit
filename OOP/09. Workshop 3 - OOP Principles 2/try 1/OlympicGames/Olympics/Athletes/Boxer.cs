﻿using OlympicGames.Olympics.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using OlympicGames.Olympics.Enums;

namespace OlympicGames.Olympics.Athletes
{
    class Boxer : Olympian,IBoxer
    {
        private int wins;
        private int losses;

        public Boxer(string firstName, string lastName, string country, BoxingCategory category, int wins, int losses)
            :base(firstName,lastName,country)
        {
            ValidateCategory(category);
            this.Wins = wins;
            this.Losses = losses;
        }
   
        public BoxingCategory Category{ get; private set; }
        public int Wins
        {
            get
            {
                return this.wins;
            }
            private set
            {
                if (value<0 || value > 100)
                {
                    throw new Exception("Wins must be between 0 and 100!");
                }
                this.wins = value;
            }
        }
        public int Losses
        {
            get
            {
                return this.losses;
            }
            private set
            {
                if (value<0 || value>100)
                {
                    throw new Exception("Losses must be between 0 and 100");
                }
                this.losses = value;
            }
        }

        private void ValidateCategory(BoxingCategory category)
        {
            bool success = Enum.IsDefined(typeof(BoxingCategory), category);
            if (!success)
            {
                throw new Exception("Category not valid");
            }
            this.Category = category;
        }
        public override string PrintAthleteInfo()
        {
            return $"BOXER: {base.PrintAthleteInfo()}\nCategory: {this.Category}\nWins: {this.wins}" +
                $"\nLosses: {this.losses}";
        }
    }
}
