﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OlympicGames.Core.Commands.Abstracts;
using OlympicGames.Core.Contracts;
using OlympicGames.Olympics.Contracts;

namespace OlympicGames.Core.Commands
{
    public class ListOlympiansCommand : Command
    {
        public ListOlympiansCommand(IList<string> commandLine, IOlympicCommittee committee)
            : base(commandLine, committee)
        {
        }

        public override string Execute()
        {
            List<IOlympian> listSorted = new List<IOlympian>();
            if (this.CommandParameters.Count > 2)
            {
                throw new ArgumentException("Parameters count is not valid!");
            }
            string key = "firstname";
            string order = "asc";

            if (this.CommandParameters.Count == 1)
            {
                key = this.CommandParameters[0];
            }
            if (this.CommandParameters.Count == 2)
            {
                key = this.CommandParameters[0];
                order = this.CommandParameters[1];
            }

            switch (order)
            {
                case "asc":
                    switch (key)
                    {
                        case "firstname":
                            listSorted = Committee.Olympians.OrderBy(x => x.FirstName).ToList(); break;
                        case "lastname":
                            listSorted = Committee.Olympians.OrderBy(x => x.LastName).ToList(); break;
                        case "country":
                            listSorted = Committee.Olympians.OrderBy(x => x.Country).ToList(); break;
                    }
                    break;
                case "desc":
                    switch (key)
                    {
                        case "firstname":
                            listSorted = Committee.Olympians.OrderByDescending(x => x.FirstName).ToList(); break;
                        case "lastname":
                            listSorted = Committee.Olympians.OrderByDescending(x => x.LastName).ToList(); break;
                        case "country":
                            listSorted = Committee.Olympians.OrderByDescending(x => x.Country).ToList(); break;
                    }
                    break;

            }
            if (this.Committee.Olympians.Count == 0)
            {
                return "NO OLYMPIANS ADDED";
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine($"Sorted by [key: {key}] in [order: {order}])");
                foreach (var item in listSorted)
                {
                    sb.AppendLine(item.ToString());
                }
                return sb.ToString().Trim();
            }

        }
    }
}
