﻿using System;
using System.Collections.Generic;
using OlympicGames.Core.Commands.Abstracts;
using OlympicGames.Core.Contracts;
using OlympicGames.Olympics.Athletes;
using OlympicGames.Olympics.Contracts;

namespace OlympicGames.Core.Commands
{
    public class CreateBoxerCommand : Command
    {
        public CreateBoxerCommand(IList<string> commandLine, IOlympicCommittee committee) 
            : base(commandLine, committee)
        {
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count!=6)
            {
                throw new ArgumentException("Parameters count is not valid");
            }
            string firstName = this.CommandParameters[0];
            string lastName = this.CommandParameters[1];
            string country = this.CommandParameters[2];
            string category = this.CommandParameters[3];

            bool isValidWins = int.TryParse(this.CommandParameters[4], out int wins);
            bool isValidLosses = int.TryParse(this.CommandParameters[5], out int losses);

            if (!isValidLosses || !isValidWins)
            {
                throw new ArgumentException("Wins and losses should be valid numbers");
            }

            IOlympian boxer = this.Factory.CreateBoxer(firstName, lastName, country, category, wins, losses);
            this.Committee.Add(boxer);
            return $"Created Boxer\n{boxer}";
        }
    }
}
