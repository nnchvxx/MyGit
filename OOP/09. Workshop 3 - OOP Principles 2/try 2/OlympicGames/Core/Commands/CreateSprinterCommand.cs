﻿using System;
using System.Collections.Generic;
using OlympicGames.Core.Commands.Abstracts;
using OlympicGames.Core.Contracts;
using OlympicGames.Olympics.Contracts;

namespace OlympicGames.Core.Commands
{
    public class CreateSprinterCommand : Command
    {
        public CreateSprinterCommand(IList<string> commandLine, IOlympicCommittee committee)
            : base(commandLine, committee)
        {
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count<3)
            {
                throw new ArgumentException("Parameters count is not valid");
            }
            string firstName = this.CommandParameters[0];
            string lastName = this.CommandParameters[1];
            string country = this.CommandParameters[2];

            IDictionary<string, double> personalRecords = new Dictionary<string, double>();
            for (int i = 3; i < this.CommandParameters.Count; i++)
            {
                string[] arr = this.CommandParameters[i].Split('/');
                bool isValidDouble = Double.TryParse(arr[1], out double result);
                if (!isValidDouble)
                {
                    throw new ArgumentException("Time not in correct format");
                }
                personalRecords.Add(arr[0], result);
            }
            IOlympian sprinter = this.Factory.CreateSprinter(firstName, lastName, country, personalRecords);
            this.Committee.Add(sprinter);
            return $"Created Sprinter\n{sprinter.ToString().Trim()}";
        }
    }
}
