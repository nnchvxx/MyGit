﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OlympicGames.Olympics.Contracts
{
    abstract class Olympian : IOlympian
    {
        private string firstName;
        private string lastName;
        private string country;

        protected Olympian(string firstName, string lastName, string country)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Country = country;
        }
        public string FirstName
        {
            get => this.firstName;
            private set
            {
                if (value.Length < 2 || value.Length > 20)
                {
                    throw new ArgumentException("First name should be between 2 and 20 characters.");
                }
                this.firstName = value;
            }
        }
        public string LastName
        {
            get => this.lastName;
            private set
            {
                if (value.Length < 2 || value.Length > 20)
                {
                    throw new ArgumentException("Last name should be between 2 and 20 characters");
                }
                this.lastName = value;
            }
        }
        public string Country
        {
            get => this.country;
            private set
            {
                if (value.Length< 3 || value.Length> 25)
                {
                    throw new ArgumentException("Country should be between 3 and 25 characters.");
                }
                this.country = value;
            }
        }

        public override string ToString()
        {
            return $"{this.firstName} {this.lastName} from {this.country}";
        }

    }


}
