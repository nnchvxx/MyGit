﻿using System;
using System.Collections.Generic;
using System.Text;
using OlympicGames.Olympics.Contracts;

namespace OlympicGames.Olympics.Athletes
{
    class Sprinter : Olympian, ISprinter
    {
        

        public Sprinter(string firstName, string lastName, string country, IDictionary<string, double> personalRecords)
            :base(firstName,lastName,country)
        {
            this.PersonalRecords = personalRecords;
        }
        public IDictionary<string, double> PersonalRecords { get; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"SPRINTER: {base.ToString()}");
            if (this.PersonalRecords.Count==0)
            {
                return sb.AppendLine("NO PERSONAL RECORDS SET").ToString().Trim();
            }
            else
            {
                sb.AppendLine("PERSONAL RECORDS:");
                foreach (var item in PersonalRecords)
                {
                    sb.AppendLine($"{item.Key}m: {item.Value}s");
                }
                return sb.ToString().Trim();
            }
        }
    }
}
