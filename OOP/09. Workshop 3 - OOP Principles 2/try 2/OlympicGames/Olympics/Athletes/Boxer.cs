﻿using System;
using System.Collections.Generic;
using System.Text;
using OlympicGames.Olympics.Contracts;
using OlympicGames.Olympics.Enums;


namespace OlympicGames.Olympics.Athletes
{
    class Boxer : Olympian, IBoxer
    {
        private BoxingCategory category;
        private int wins;
        private int losses;

        public Boxer(string firstName, string lastName, string country, BoxingCategory category, int wins, int losses)
            : base(firstName,lastName,country)
        {
            this.category = category;
            this.Wins = wins;
            this.Losses = losses;
        }
        public BoxingCategory Category { get; }
        public int Wins
        {
            get => this.wins;
            private set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentException("Wins must be between 0 and 100");
                }
                this.wins = value;
            }
        }
        public int Losses
        {
            get => this.losses;
            private set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentException("Losses must be between 0 and 100");
                }
                this.losses = value;
            }
        }

        public override string ToString()
        {
            return $"BOXER: {base.ToString()}\nCategory: {this.category}\nWins: {this.wins}\nLosses: {this.losses}";
        }

    }
}
