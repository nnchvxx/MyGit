﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQify
{
    public static class Helper
    {
        public static List<Person> GetData()
        {
            return new List<Person>
            {
                new Person
                {
                    firstName = "Bowman",
                    lastName = "Parks",
                    age = 21,
                    eyeColor = "blue"
                },
                new Person
                {
                    firstName = "Eaton",
                    lastName = "Mcconnell",
                    age = 49,
                    eyeColor = "green"
                },
                new Person
                {
                    firstName = "Alfreda",
                    lastName = "Dixon",
                    age = 46,
                    eyeColor = "brown"
                },
                new Person
                {
                    firstName = "Terry",
                    lastName = "Hickman",
                    age = 28,
                    eyeColor = "blue"
                },
                new Person
                {
                    firstName = "Byers",
                    lastName = "Henderson",
                    age = 47,
                    eyeColor = "brown"
                },
                new Person
                {
                    firstName = "Anita",
                    lastName = "Haynes",
                    age = 27,
                    eyeColor = "brown"
                },
                new Person
                {
                    firstName = "Walls",
                    lastName = "Mendez",
                    age = 42,
                    eyeColor = "green"
                },
                new Person
                {
                    firstName = "Kellie",
                    lastName = "Clayton",
                    age = 21,
                    eyeColor = "brown"
                },
                new Person
                {
                    firstName = "Dorothea",
                    lastName = "Boyle",
                    age = 50,
                    eyeColor = "blue"
                },
                new Person
                {
                    firstName = "Jacqueline",
                    lastName = "Brennan",
                    age = 45,
                    eyeColor = "green"
                },
                new Person
                {
                    firstName = "Sloan",
                    lastName = "Ruiz",
                    age = 35,
                    eyeColor = "green"
                },
                new Person
                {
                    firstName = "Florence",
                    lastName = "Keith",
                    age = 38,
                    eyeColor = "brown"
                },
                new Person
                {
                    firstName = "Gordon",
                    lastName = "Gibbs",
                    age = 21,
                    eyeColor = "green"
                },
                new Person
                {
                    firstName = "Butler",
                    lastName = "Sargent",
                    age = 33,
                    eyeColor = "blue"
                },
                new Person
                {
                    firstName = "Powell",
                    lastName = "Clay",
                    age = 25,
                    eyeColor = "green"
                },
                new Person
                {
                    firstName = "Charity",
                    lastName = "Levine",
                    age = 32,
                    eyeColor = "blue"
                },
                new Person
                {
                    firstName = "Shauna",
                    lastName = "Harris",
                    age = 25,
                    eyeColor = "brown"
                },
                new Person
                {
                    firstName = "Avila",
                    lastName = "Johnson",
                    age = 34,
                    eyeColor = "green"
                },
                new Person
                {
                    firstName = "Juanita",
                    lastName = "Waller",
                    age = 32,
                    eyeColor = "brown"
                },
                new Person
                {
                    firstName = "Maribel",
                    lastName = "Hartman",
                    age = 46,
                    eyeColor = "blue"
                },
                new Person
                {
                    firstName = "Virgie",
                    lastName = "Castaneda",
                    age = 42,
                    eyeColor = "blue"
                },
                new Person
                {
                    firstName = "Dee",
                    lastName = "Cooley",
                    age = 18,
                    eyeColor = "green"
                },
                new Person
                {
                    firstName = "Marietta",
                    lastName = "Mays",
                    age = 19,
                    eyeColor = "green"
                },
                new Person
                {
                    firstName = "House",
                    lastName = "Gross",
                    age = 40,
                    eyeColor = "blue"
                },
                new Person
                {
                    firstName = "Copeland",
                    lastName = "Hester",
                    age = 37,
                    eyeColor = "green"
                },
                new Person
                {
                    firstName = "Serrano",
                    lastName = "Wynn",
                    age = 26,
                    eyeColor = "blue"
                },
                new Person
                {
                    firstName = "Inez",
                    lastName = "Donaldson",
                    age = 45,
                    eyeColor = "green"
                },
                new Person
                {
                    firstName = "Johns",
                    lastName = "Neal",
                    age = 34,
                    eyeColor = "green"
                },
                new Person
                {
                    firstName = "Spencer",
                    lastName = "Aguilar",
                    age = 48,
                    eyeColor = "blue"
                },
                new Person
                {
                    firstName = "Price",
                    lastName = "Molina",
                    age = 30,
                    eyeColor = "brown"
                },
                new Person
                {
                    firstName = "Beatrice",
                    lastName = "Mullins",
                    age = 46,
                    eyeColor = "brown"
                },
                new Person
                {
                    firstName = "Jodi",
                    lastName = "King",
                    age = 31,
                    eyeColor = "brown"
                }
            };
        }
    }

    public class Person
    {
        public string firstName;
        public string lastName;
        public int age;
        public string eyeColor;
    }
}
