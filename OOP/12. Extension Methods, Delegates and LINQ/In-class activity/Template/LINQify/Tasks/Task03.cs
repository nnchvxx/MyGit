﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQify.Tasks
{
    public static class Task03
    {
        //Task 03 - Filter the people who are over 30 and then sum their age

        public static int Execute(List<Person> people)
        {
            int totalAge = 0;

            for (int i = 0; i < people.Count; i++)
            {
                if (people[i].age > 30)
                {
                    totalAge += people[i].age;
                }
            }

            return totalAge;
        }

        public static int ExecuteWithLINQ(List<Person> people)
        {
            return people.Where(p => p.age > 30).Sum(p => p.age);
        }
    }
}
