﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQify.Tasks
{
    public static class Task12
    {
        //Task 12 - Find the people who are under or are 55 and their eye color is green. 
        //Then order them by first name, then by last name and add them to the result list until a person's last name ends with 'z'

        public static List<Person> Execute(List<Person> people)
        {
            var filteredPeople = new List<Person>();

            for (int i = 0; i < people.Count; i++)
            {
                if (people[i].age <= 55 && people[i].eyeColor == "green")
                {
                    filteredPeople.Add(people[i]);
                }
            }

            filteredPeople.Sort((a, b) =>
            {
                if (a.firstName.CompareTo(b.firstName) != 0)
                {
                    return a.firstName.CompareTo(b.firstName);
                }
                else
                {
                    return a.lastName.CompareTo(b.lastName);
                }
            });

            var result = new List<Person>();

            for (int i = 0; i < filteredPeople.Count; i++)
            {
                if (filteredPeople[i].lastName.EndsWith('z'))
                {
                    break;
                }
                result.Add(filteredPeople[i]);
            }

            return result;
        }

        public static List<Person> ExecuteWithLINQ(List<Person> people)
        {
            return people.Where(p => p.age <= 55 && p.eyeColor == "green").OrderBy(p => p.firstName).ThenBy(p => p.lastName).TakeWhile(p => !p.lastName.EndsWith("z")).ToList();
        }
    }
}
