﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQify.Tasks
{
    public static class Task11
    {
        //Task 11 - Return a collection ordered by first name and then ordered by last name.

        public static List<Person> Execute(List<Person> people)
        {
            var result = new List<Person>(people);

            result.Sort((a, b) =>
            {
                if (a.firstName.CompareTo(b.firstName) != 0)
                {
                    return a.firstName.CompareTo(b.firstName);
                }
                else
                {
                    return a.lastName.CompareTo(b.lastName);
                }
            });

            return result;
        }

        public static List<Person> ExecuteWithLINQ(List<Person> people)
        {
            return people.OrderBy(p => p.firstName).ThenBy(p => p.lastName).ToList();
        }
    }
}
