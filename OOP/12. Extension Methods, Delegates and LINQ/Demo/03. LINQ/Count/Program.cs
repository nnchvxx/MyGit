﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Count
{
    class Program
    {
        static void Main(string[] args)
        {
            //Returns the number of elements in a sequence.

            int[] array = { 1, 2, 3, 4, 5 };

            Console.WriteLine(array.Count());
        }
    }
}
