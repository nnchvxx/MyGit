﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Academy.Collections.Generic
{
    public class MyList<T> : IEnumerable<T>
    {
        private T[] items;
        private int length = 0;

        public MyList()
        {
            items = new T[4];
        }

        public MyList(int length)
        {
            items = new T[length];
        }

        public int Count
        {
            get
            {
                return this.length;
            }
        }

        public int Capacity
        {
            get
            {
                return this.items.Length;
            }
            set
            {
                this.items = new T[value];
               
            }
        }

        public T this[int i]
        {
            get { return this.items[i]; }
            set
            {
                this.items[i] = value;
                this.length++;
            }
        }

        /// <summary>
        /// Adds an item to the collection.
        /// </summary>
        /// <param name="value">The object to be added to the collection.</param>
        /// <returns>void</returns>
        public void Add(T value)
        {
            if (length == this.items.Length)
            {
                Resize();
            }
            this.items[this.length] = value;

            this.length++;
        }

        private void Resize()
        {
            var temp = this.items;

            if (this.items.Length == length)
            {
                items = new T[this.items.Length * 2];

                for (int i = 0; i < length; i++)
                {
                    items[i] = temp[i];
                }
            }
            else if(length <= this.items.Length / 2)
            {
                items = new T[this.items.Length / 2];

                for (int i = 0; i < length; i++)
                {
                    items[i] = temp[i];
                }
            }
        }

        public void Resize(int size)
        {
            var temp = new T[size];

            if (size >= this.items.Length)
            {
                for (int i = 0; i < this.items.Length; i++)
                {
                    temp[i] = this.items[i];
                }
            }
            else
            {
                for (int i = 0; i < size; i++)
                {
                    temp[i] = this.items[i];
                }
            }

            this.items = temp;
        }

        public bool Remove(T item)
        {
            bool isRemoved = false;

            var newArr = new T[length - 1];

            int counter = 0;

            for (int i = 0; i < length; i++)
            {
                if (this.items[i].Equals(item) && isRemoved == false)
                {
                    isRemoved = true;
                    continue;
                }

                newArr[counter] = this.items[i];

                counter++;
            }
            this.items = newArr;

            length--;

            return isRemoved;
        }


        public int IndexOf(T item)
        {
            int indexOf = -1;

            for (int i = 0; i < length; i++)
            {
                if (this.items[i].Equals(item))
                {
                    indexOf = i;
                    break;
                }
            }

            return indexOf;
        }

        public int LastIndexOf(T item)
        {
            int indexOf = -1;

            for (int i = length - 1; i >= 0; i--)
            {
                if (this.items[i].Equals(item))
                {
                    indexOf = i;
                    break;
                }
            }

            return indexOf;
        }

        public void RemoveAt(int index)
        {
            var newArr = new T[length - 1];

            int counter = 0;

            for (int i = 0; i < length; i++) 
            {
                if (i == index)
                {
                    continue;
                }

                newArr[counter] = this.items[i];

                counter++;
            }
            this.items = newArr;

            length--;
        }

        public void Clear()
        {
            this.items = new T[4];
            length = 0;
        }

        public void Populate(T value)
        {
            for (int i = 0; i < this.items.Length; i++)
            {
                this.items[i] = value;
                length++;
            }
        }

        public void Swap(int index0, int index1)
        {
            T temp = this.items[index0];

            this.items[index0] = this.items[index1];
            this.items[index1] = temp;
        }

        public bool Contains(T value)
        {
            for (int i = 0; i < length; i++)
            {
                if (this.items[i].Equals(value))
                {
                    return true;
                }
            }

            return false;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return this.items.Take(this.length).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.items.Take(this.length).GetEnumerator();
        }
    }
}

