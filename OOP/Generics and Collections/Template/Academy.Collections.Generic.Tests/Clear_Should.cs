﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Academy.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Academy.Collections.Generic.Tests
{
    [TestClass]
    public class Clear_Should
    {
        [TestMethod]
        public void CorrectlyClearsList()
        {
            //Arrange
            var list = new MyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            //Act
            list.Clear();

            //Assert
            Assert.AreEqual(0, list.Count);
        }
    }
}
