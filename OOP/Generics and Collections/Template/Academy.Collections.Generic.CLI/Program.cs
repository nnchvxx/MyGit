﻿using Academy.Collections.Generic;
using System;
using System.Collections.Generic;

namespace Academy.Collections.Generic.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            var list = new MyList<string>(6);

            //list.Add("b");
            //list.Add("a");
            //list.Add("t");
            //list.Add("m");
            //list.Add("a");
            //list.Add("n");

            list.Populate("bla");
            list.Resize(3);

            Console.WriteLine(string.Join(", ", list));

        }
    }
}
