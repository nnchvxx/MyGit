﻿using System.Collections.Generic;

using CosmeticsShop.Core;
using CosmeticsShop.Models;
using System;

namespace CosmeticsShop.Commands
{
	public class CreateCategory : ICommand
	{
		private readonly ProductRepository productRepository;
		private readonly ProductFactory productFactory;
		private string result;

		public CreateCategory(ProductRepository productRepository, ProductFactory productFactory)
		{
			this.productRepository = productRepository;
			this.productFactory = productFactory;
		}

		public void Execute(List<string> parameters)
		{
			if (parameters.Count != 1)
            {
				throw new Exception("Category can accept one name.");
            }

			string categoryName = parameters[0];

			if (productRepository.Categories.ContainsKey(categoryName))
            {
				throw new Exception($"Category {categoryName} already exists.");
            }

			Category category = this.productFactory.CreateCategory(categoryName);
			this.productRepository.Categories.Add(categoryName, category);

			this.result = $"Category with name {categoryName} was created!";
		}

		public string Result
		{
			get
			{
				return this.result;
			}
		}
	}
}
