﻿using System.Collections.Generic;

namespace CosmeticsShop.Commands
{
	public interface ICommand
	{
		public void Execute(List<string> parameters);

		string Result
		{
			get;
		}
	}
}
