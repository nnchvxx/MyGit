﻿using System.Collections.Generic;
using CosmeticsShop.Core;
using CosmeticsShop.Models;
using System;
namespace CosmeticsShop.Commands
{
    public class AddProductToCategory : ICommand
    {
        private readonly ProductRepository productRepository;
        private string result;

        public AddProductToCategory(ProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        public void Execute(List<string> parameters)
        {
            string categoryName = "";
            string productName = "";

            if (parameters.Count == 2)
            {
                categoryName = parameters[0];
                productName = parameters[1];
            }

            if (!this.productRepository.Categories.ContainsKey(categoryName))
            {
                throw new Exception($"Category {categoryName} does not exist!");
            }

            Category category = this.productRepository.Categories[categoryName];

            if (!this.productRepository.Products.ContainsKey(productName))
            {
                throw new Exception($"Product {productName} does not exist!");
            }
            

            Product product = this.productRepository.Products[productName];

            category.AddProduct(product);

            this.result = $"Product {productName} added to category {categoryName}!";
        }
        public string Result
        {
            get
            {
                return this.result;
            }
        }
    }
}
