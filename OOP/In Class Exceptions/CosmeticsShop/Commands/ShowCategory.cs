﻿using CosmeticsShop.Core;
using CosmeticsShop.Models;
using System.Collections.Generic;
using System;

namespace CosmeticsShop.Commands
{
	public class ShowCategory : ICommand
	{
		private readonly ProductRepository productRepository;
		private string result;

		public ShowCategory(ProductRepository productRepository)
		{
			this.productRepository = productRepository;
		}

		public void Execute(List<string> parameters)
		{
			if (parameters.Count != 1) 
			{
				throw new Exception("Invalid parameters count!");
			}

			string categoryName = parameters[0];

			if (!this.productRepository.Categories.ContainsKey(categoryName))
            {
				throw new ArgumentException("Category does not exist!");
            }

			Category category = this.productRepository.Categories[categoryName];

			this.result = category.Print();
		}

		public string Result
		{
			get
			{
				return this.result;
			}
		}
	}
}
