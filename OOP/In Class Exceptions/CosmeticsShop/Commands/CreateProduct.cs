﻿using System;
using System.Collections.Generic;
using CosmeticsShop.Core;
using CosmeticsShop.Models;

namespace CosmeticsShop.Commands
{
	public class CreateProduct : ICommand
	{
		private readonly ProductRepository productRepository;
		private readonly ProductFactory productFactory;
		private string result;

		public CreateProduct(ProductRepository productRepository, ProductFactory productFactory)
		{
			this.productRepository = productRepository;
			this.productFactory = productFactory;
		}

		public void Execute(List<string> parameters)
		{
			if (parameters.Count != 4)
			{
				throw new Exception("CreateProduct command expects 4 parameters.");
			}
			string name = parameters[0];
			string brand = parameters[1];

			double price;

			bool isValidPrice = double.TryParse(parameters[2], out price);

			if (!isValidPrice)
            {
				throw new FormatException("Third parameter should be price (real number).");
            }

			if (!Enum.IsDefined(typeof(GenderType), parameters[3]))
            {
				throw new Exception("Fourth parameter should be one of Men, Women or Unisex.");
            }

			GenderType gender = (GenderType)Enum.Parse(typeof(GenderType), parameters[3], true);

			if (this.productRepository.Products.ContainsKey(name))
            {
				throw new Exception($"Product {name} already exists.");
            }

			Product product = this.productFactory.CreateProduct(name, brand, price, gender);
			this.productRepository.Products.Add(name, product);

			this.result = $"Product with name {name} was created!";
		}

		public string Result
		{
			get
			{
				return this.result;
			}
		}
	}
}
