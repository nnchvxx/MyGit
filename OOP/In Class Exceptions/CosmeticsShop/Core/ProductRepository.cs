﻿using CosmeticsShop.Models;

using System.Collections.Generic;
namespace CosmeticsShop.Core
{
	public class ProductRepository
	{
		private readonly Dictionary<string, Category> categories;
		private readonly Dictionary<string, Product> products;

		public ProductRepository()
		{
			this.categories = new Dictionary<string, Category>();
			this.products = new Dictionary<string, Product>();
		}

		public Dictionary<string, Category> Categories
		{
			get
			{
				return this.categories;
			}
		}

		public Dictionary<string, Product> Products
		{
			get
			{
				return this.products;
			}
		}
	}
}
