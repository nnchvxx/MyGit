﻿using System;

namespace CosmeticsShop.Core
{
	public class ConsoleReader
	{
		public string ReadLine()
		{
			return Console.ReadLine();
		}
	}
}
