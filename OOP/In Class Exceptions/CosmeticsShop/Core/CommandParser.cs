﻿using System.Collections.Generic;

namespace CosmeticsShop.Core
{
	public class CommandParser
	{
		public string ParseCommand(string commandLine)
		{
			string commandName = commandLine.Split(" ")[0];
			return commandName;
		}

		public List<string> ParseParameters(string commandLine)
		{
			string[] commandParts = commandLine.Split(" ");
			List<string> parameters = new List<string>();
			for (int i = 1; i < commandParts.Length; i++)
			{
				parameters.Add(commandParts[i]);
			}
			return parameters;
		}
	}
}
