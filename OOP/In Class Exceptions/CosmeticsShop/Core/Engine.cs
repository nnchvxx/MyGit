﻿using CosmeticsShop.Commands;
using System;
using System.Collections.Generic;

namespace CosmeticsShop.Core
{
	public class Engine
	{
		private const string TERMINATION_COMMAND = "Exit";

		private readonly CommandParser commandParser;
		private readonly CommandFactory commandFactory;
		private readonly ProductFactory productFactory;
		private readonly ProductRepository productRepository;
		private readonly ConsoleReader reader;
		private readonly ConsoleWriter writer;

		public Engine()
		{
			this.commandParser = new CommandParser();
			this.commandFactory = new CommandFactory();
			this.productFactory = new ProductFactory();
			this.productRepository = new ProductRepository();
			this.reader = new ConsoleReader();
			this.writer = new ConsoleWriter();
		}

		public void Start()
		{
			while (true)
			{
				string commandLine = this.reader.ReadLine();
				if (commandLine == TERMINATION_COMMAND)
					break;

                try
                {
					this.ProcessCommand(commandLine);
				}
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
			}
		}

		private void ProcessCommand(string commandLine)
		{
			string commandName = this.commandParser.ParseCommand(commandLine);
			List<string> parameters = this.commandParser.ParseParameters(commandLine);
			ICommand command = this.commandFactory.CreateCommand(commandName, this.productFactory, this.productRepository);
			command.Execute(parameters);
			this.writer.WriteLine(command.Result);
		}
	}

}
