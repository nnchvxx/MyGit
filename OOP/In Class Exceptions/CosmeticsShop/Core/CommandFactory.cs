﻿using CosmeticsShop.Commands;
using System;

namespace CosmeticsShop.Core
{
	public class CommandFactory
	{
		public ICommand CreateCommand(string commandTypeValue, ProductFactory productFactory, ProductRepository productRepository)
		{
            bool isCommandValid = Enum.IsDefined(typeof(CommandType), commandTypeValue.ToUpper());

			if (!isCommandValid)
            {
				throw new ArgumentException($"Command {commandTypeValue} is not supported.");
            }

			CommandType commandType = (CommandType)Enum.Parse(typeof(CommandType), commandTypeValue.ToUpper(), false);

			switch (commandType)
			{
				case CommandType.CREATECATEGORY:
					return new CreateCategory(productRepository, productFactory);
				case CommandType.CREATEPRODUCT:
					return new CreateProduct(productRepository, productFactory);
				case CommandType.ADDPRODUCTTOCATEGORY:
					return new AddProductToCategory(productRepository);
				case CommandType.SHOWCATEGORY:
					return new ShowCategory(productRepository);
				default:
					throw new NotImplementedException("Not implemented command type!");
					
			}
		}
	}
}
