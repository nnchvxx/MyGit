﻿using CosmeticsShop.Models;

namespace CosmeticsShop.Core
{
	public class ProductFactory
	{
		public Category CreateCategory(string name)
		{
			return new Category(name);
		}

		public Product CreateProduct(string name, string brand, double price, GenderType gender)
		{
			return new Product(name, brand, price, gender);
		}
	}
}
