﻿using System;
using System.Collections.Generic;

namespace GameCreatures.Models
{
    public class Commander
    {
        private readonly List<Creature> army;
        private string name;

        public Commander(string name, List<Creature> army)
        {
            if (name==null)
            {
                throw new ArgumentNullException("Please enter valid name"); 
            }
            if (army==null)
            {
                throw new ArgumentNullException("Please enter valid army");
            }
            this.name = name;
            this.army = army;
        }

        public string Name
        {
            get
            {
                return this.name;
            }
        }

        public int ArmySize
        {
            get
            {
                return this.army.Count;
            }
        }

        public void AttackAtPosition(Commander enemy, int attackerIndex, int targetIndex)
        {
            this.army[attackerIndex].Attack(enemy.army[targetIndex]);
            if (enemy.army[targetIndex].HealthPoints==0)
            {
                enemy.army.RemoveAt(targetIndex);
            }
            
        }

        public void AutoAttack(Commander enemy)
        {
            Creature attacker = this.army[0];
            Creature attacked = enemy.army[0];
            int minHp = int.MaxValue;

            foreach (Creature item in this.army)
            {
                Creature creature = item.FindBestTarget(enemy.army);
                int currentHp = creature.HealthPoints - item.CalculateActualDamage(creature);
                if (currentHp<0)
                {
                    currentHp = 0;
                }
                if (currentHp<minHp)
                {
                    minHp = currentHp;
                    attacker = item;
                    attacked = creature;
                }
                if (currentHp==minHp)
                {
                    if (creature.Damage>item.Damage)
                    {
                        attacked = creature;
                        attacker = item;
                    }
                }
            }
            attacker.Attack(attacked);
            if (attacked.HealthPoints == 0)
            {
                enemy.army.Remove(attacked);
            }

        }
    }
}
