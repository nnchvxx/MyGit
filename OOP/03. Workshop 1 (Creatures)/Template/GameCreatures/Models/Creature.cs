﻿using System;
using System.Collections.Generic;

namespace GameCreatures.Models
{
    public class Creature
    {
        private string name;
        private int damage;
        private int healthPoints;

        public Creature(
            string name,
            int damage,
            int healthPoints,
            AttackType attackType,
            ArmorType armorType)
        {
            if (name==null)
            {
                throw new ArgumentNullException("Please enter valid name.");
            }
            if (damage<=0)
            {
                throw new ArgumentOutOfRangeException("Please enter valid damage.");
            }
            if (healthPoints<=0)
            {
                throw new ArgumentOutOfRangeException("Please enter valid health points");
            }
            this.name = name;
            this.damage = damage;
            this.healthPoints = healthPoints;
            this.AttackType = attackType;
            this.ArmorType = armorType;
        }
        public string Name 
        { 
            get 
            {
                return this.name; 
            } 
        }
        public int Damage 
        {
            get 
            {
                return this.damage; 
            } 
        }
        public int HealthPoints
        {
            get
            {
                return this.healthPoints;
            }
            set
            {
                if (value <= 0)
                {
                    this.healthPoints = 0;
                }
                else
                {
                    this.healthPoints = value;
                }
            }
        }
        public ArmorType ArmorType { get; set; }
        public AttackType AttackType { get; set; }
        public void Attack(Creature target)
        {
            target.HealthPoints -= CalculateActualDamage(target);
        }
        public Creature FindBestTarget(List<Creature> targets)
        {
            int lowestHp = int.MaxValue;
            for (int i = 0; i < targets.Count; i++)
            {
                int check = targets[i].HealthPoints - CalculateActualDamage(targets[i]);
                if (check<0)
                {
                    check = 0;
                }
                if (check<lowestHp)
                {
                    lowestHp = check;
                }
            }
            int maxDamage = 0;
            int indexMaxDamage = 0;
            for (int i = 0; i < targets.Count; i++)
            {
                int check = targets[i].HealthPoints - CalculateActualDamage(targets[i]);
                if (check<0)
                {
                    check = 0;
                }
                if (lowestHp == check)
                {
                    if (targets[i].Damage>maxDamage)
                    {
                        maxDamage = targets[i].Damage;
                        indexMaxDamage = i;
                    }
                }
            }
            return targets[indexMaxDamage];
        }
        public void AutoAttack(List<Creature> targets)
        {
            this.Attack(FindBestTarget(targets));
        }
        public int CalculateActualDamage(Creature target)
        {
            AttackType attackType = this.AttackType;
            ArmorType armorType = target.ArmorType;
            if (armorType == ArmorType.Light)
            {
                if (attackType == AttackType.Ranged)
                {
                    return (int)Math.Floor(this.Damage * 1.25);
                }
                if (attackType == AttackType.Melee)
                {
                    return (int)Math.Floor(this.Damage * 1.0);
                }
                if (attackType == AttackType.Magic)
                {
                    return (int)Math.Floor(this.Damage * 0.75);
                }
            }
            else if (armorType == ArmorType.Medium)
            {
                if (attackType == AttackType.Ranged)
                {
                    return (int)Math.Floor(this.Damage * 1.0);
                }
                if (attackType == AttackType.Melee)
                {
                    return (int)Math.Floor(this.Damage * 1.25);
                }
                if (attackType == AttackType.Magic)
                {
                    return (int)Math.Floor(this.Damage * 1.0);
                }
            }
            else if (armorType == ArmorType.Heavy)
            {
                if (attackType == AttackType.Ranged)
                {
                    return (int)Math.Floor(this.Damage * 0.75);
                }
                if (attackType == AttackType.Melee)
                {
                    return(int)Math.Floor(this.Damage * 0.75);
                }
                if (attackType == AttackType.Magic)
                {
                    return (int)Math.Floor(this.Damage * 1.25);
                }
            }
            return 0;
        }
    }
}
